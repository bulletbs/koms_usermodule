$(function(){
    var base_uri = '/user/';

    $(document).ajaxStart(function() { $('#loading_layer').show(); });
    $(document).ajaxStop(function() { $('#loading_layer').hide(); });

    /* REegion handlers */
    $('#region').change(function(){
        loadRegionCities();
    });
    $(document).on('change', '#city', function(){
        $('#city_id').val( $(this).val() );
    });
    setStyle();

    function loadRegionCities(){
        var val = $('#region').val();
        if(val  > 0){
            $.ajax({
                url: base_uri + "ajax_cities",
                type: "POST",
                dataType: "json",
                data: {'selectedRegion':val }
            })
            .done(function(data) {
                $('#subRegion').html(data.cities != '' ?  '&nbsp;&nbsp;<b>&raquo;</b>&nbsp;&nbsp;' + data.cities : '');
                $('#city_id').val( null );
                setStyle();
            });
        }
        else{
            $('#subRegion').html('');
            $('#city_id').val( null );
        }

    }

    function setStyle(){
        if ($.fn.styler) {
            $('input, select').styler({});
        }
    }
});
