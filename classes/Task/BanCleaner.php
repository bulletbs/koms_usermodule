<?php defined('SYSPATH') or die('No direct script access.');

/**
 * MInion task for clean banned users
 */
class Task_BanCleaner extends Minion_Task
{
    /**
     * Generate sitemaps
     */
    protected function _execute(Array $params){
        Kohana::$environment = !isset($_SERVER['windir']) && !isset($_SERVER['GNOME_DESKTOP_SESSION_ID']) ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;
        $start = time();

        $domains = Kohana::$config->load('users.banned_domains');
        foreach($domains as $domain){
            $users = ORM::factory('User')->where('email', 'LIKE', '%'.$domain)->find_all();
            foreach($users as $user)
                $user->delete();
        }

        print 'Operation taken -'. (time() - $start) .' seconds for delete '. count($users). ' users'.PHP_EOL;
    }
}