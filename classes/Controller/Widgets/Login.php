<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Widgets_Login extends Controller_System_Widgets{

    public $template = 'widgets/login';

    public function action_index()
    {
        $this->template->set(array(
            'favorites' => isset($_COOKIE['board_favorites']) && count($_COOKIE['board_favorites']) ? count($_COOKIE['board_favorites']) : 0,
            'user' => $this->current_user,
            'unreaded' => Auth::instance()->logged_in('login') ? Model_UserDialog::lookForMessages($this->current_user->id) : 0,
        ));
    }
}