<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Виджет "Меню админа"
 */
class Controller_Widgets_ProfileMenu extends Controller_System_Widgets {

    public $template = 'widgets/profile_menu';    // Шаблон виждета

    public function action_index()
    {
        $modules = Kohana::modules();
        $controller = lcfirst(Request::initial()->controller());
        $action = Request::initial()->action();

        $menu = array();
        if(isset($modules['board'])){
            $defaults = Route::get('board_myads')->defaults();
            $menu[] =
                array(
                    'label' => __('My ads'),
                    'uri' => Route::get('board_myads')->uri(),
                    'active' => $controller == $defaults['controller'],
                );
        }
        if(isset($modules['catalog']) && Auth::instance()->logged_in('company')){
            $defaults = Route::get('catalog_mycompany')->defaults();
            $menu[] =
                array(
                    'label' => __('My company'),
                    'uri' => Route::get('catalog_mycompany')->uri(),
                    'active' => $controller == $defaults['controller'],
                );
        }
        $menu[] = array(
            'label' => __('My messages'),
            'uri' => Route::get('auth')->uri(['action'=>'mail']),
            'active' => $controller == 'userInbox',
        );
        $menu[] = array(
            'label' => __('Change profile'),
            'uri' => Route::get('auth')->uri(['action'=>'profile']),
            'active' => $action == 'profile',
        );
        $menu[] = array(
            'label' => __('Change password'),
            'uri' => Route::get('auth')->uri(['action'=>'password']),
            'active' => $action == 'password',
        );
        $menu[] = array(
            'label' => __('Change email'),
            'uri' => Route::get('auth')->uri(['action'=>'change_mail']),
            'active' => $action == 'change_mail',
        );
        $menu[] = array(
            'label' => __('Delete profile'),
            'uri' => Route::get('auth')->uri(['action'=>'remove_account']),
            'active' => $action == 'remove_account',
        );
        $menu[] = array(
            'label' => __('Logout'),
            'uri' => Route::get('auth')->uri(['action'=>'logout']),
            'active' => $action == 'logout',
        );

        // Вывод в шаблон
        $this->template->menu = $menu;
    }
}