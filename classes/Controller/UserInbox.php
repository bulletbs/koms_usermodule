<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Users inside mail Controller
 * @class Controller_UserBox
 */
class Controller_UserInbox extends Controller_User
{
    public $auth_required = 'login';

    public $skip_auto_content_apply = array(
        'send',
        'delete',
        'pack',
        'unpack',
    );

    public function before(){
        if(!in_array($this->request->action(), $this->skip_auto_content_apply)){
            $this->uri = 'inbox/'.$this->request->action();
        }
        parent::before();
        $this->styles[] = "assets/user/css/chat.css";
        $this->breadcrumbs->add(__('My messages'), Route::get('messaging')->uri());
    }


    /**
     * Inbox folder
     */
    public function action_index(){
        $dialogs = ORM::factory('UserDialog')

            ->where_open()
            ->where_open()
            ->where('user_id', '=', $this->current_user->id)
            ->and_where('user_archive', '=', 0)
            ->where_close()
            ->or_where_open()
            ->where('opponent_id', '=', $this->current_user->id)
            ->and_where('opponent_archive', '=', 0)
            ->or_where_close()
            ->or_where_close()
            ->and_where('last_message_user', '<>', $this->current_user->id)

//            ->where('opponent_id', '=', $this->current_user->id)
//            ->and_where('opponent_archive', '=', 0)
            ->order_by('last_message_time', 'DESC')
            ->find_all();
        $this->user_content->set(array(
            'title' => __('Received messages'),
            'dialogs' => $dialogs,
            'active' => 'inbox',
            'myId' => $this->current_user->id,
        ));
    }

    /**
     * Outbox folder
     * @throws Kohana_Exception
     */
    public function action_sent(){
        $dialogs = ORM::factory('UserDialog')

            ->where_open()
            ->where_open()
            ->where('user_id', '=', $this->current_user->id)
            ->and_where('user_archive', '=', 0)
            ->where_close()
            ->or_where_open()
            ->where('opponent_id', '=', $this->current_user->id)
            ->and_where('opponent_archive', '=', 0)
            ->or_where_close()
            ->or_where_close()
            ->and_where('last_message_user', '=', $this->current_user->id)

//            ->where('user_id', '=', $this->current_user->id)
//            ->and_where('user_archive', '=', 0)
            ->order_by('last_message_time', 'DESC')
            ->find_all();
        $this->user_content = $this->getContentTemplate('inbox/index')->set(array(
            'title' => __('Sent messages'),
            'dialogs' => $dialogs,
            'active' => 'sent',
            'myId' => $this->current_user->id,
        ));
    }

    /**
     * Outbox folder
     * @throws Kohana_Exception
     */
    public function action_archive(){
        $dialogs = ORM::factory('UserDialog')
            ->where_open()
            ->where('user_id', '=', $this->current_user->id)
            ->and_where('user_archive', '=', 1)
            ->where_close()
            ->or_where_open()
            ->where('opponent_id', '=', $this->current_user->id)
            ->and_where('opponent_archive', '=', 1)
            ->or_where_close()
            ->order_by('last_message_time', 'DESC')
            ->find_all();
        $this->user_content = $this->getContentTemplate('inbox/index')->set(array(
            'title' => __('Archived messages'),
            'dialogs' => $dialogs,
            'active' => 'archive',
            'myId' => $this->current_user->id,
        ));
    }

    /**
     * Dialog chat
     */
    public function action_dialog(){
        $dialog = $this->_getCurrentDialog();

        $user_id = $this->current_user->id;
        $messages = $dialog->messages->order_by('sendtime', 'ASC')->find_all();
        $users[$dialog->user_id] = $user_id == $dialog->user_id ? $this->current_user : $dialog->user;
        $users[$dialog->opponent_id] = $user_id == $dialog->opponent_id ? $this->current_user : $dialog->opponent;
        if($this->is_mobile){

        }
        else
            $this->styles[] = "assets/user/css/chat.css";

        if($dialog->user_id == $user_id  && $dialog->user_unread)
            $dialog->values(array('user_unread'=>0))->update();
        if($dialog->opponent_id == $user_id && $dialog->opponent_unread)
            $dialog->values(array('opponent_unread'=>0))->update();

        $this->user_content->set(array(
            'dialog' => $dialog,
            'messages' => $messages,
            'users' => $users,
            'myId' => $user_id,
        ));
    }

    /**
     * Send a message action
     */
    public function action_send(){
        $dialog = $this->_getCurrentDialog();
        try{
            $text = Arr::get($_POST, 'text');
            $dialog->addMessage($this->current_user->id, $text);
            Flash::success(__('Your message successfully added'));

            /* Notifying user about new messages (if no_mails flag is false) */
            $opponent = ORM::factory('User', $dialog->getOpponentId($this->current_user->id));
            if($opponent->loaded() && !$opponent->no_mails){
                $message = View::factory('board/mail/user_message_notify', array(
                    'name' => $opponent->profile->name,
                    'title'=> $dialog->subject,
                    'dialog_link'=> URL::site(Model_User::generateCryptoLink('messaging', $opponent->id, array('dialog_id' => $dialog->id)), KoMS::protocol()),
                    'site_name'=> $this->config['project']['name'],
                    'server_name'=> URL::base(KoMS::protocol()),
                    'unsubscribe_link'=> Model_User::generateCryptoLink('unsubscribe', $opponent->id),
                ))->render();
//                file_put_contents(DOCROOT. '/debug_mail.txt', PHP_EOL.PHP_EOL. $message, FILE_APPEND);
                    Email::instance()
                        ->to($opponent->email)
                        ->from($this->config['robot_email'])
                        ->subject($this->config['project']['name'] .': '. __('Message from bulletin board'))
                        ->message($message, true)
                        ->send();
            }

        }
        catch(ORM_Validation_Exception $e){
            $errors = $e->errors('messages/validation');
            foreach($errors as $_error)
                Flash::error($_error);
        }
        $this->redirect( Route::get('messaging')->uri(array('action'=>'dialog', 'id'=>$dialog->id)) .'#form' );
    }

    /**
     * Delete dialog
     */
    public function action_delete(){
        $dialog = $this->_getCurrentDialog();
        $dialog->delete();
        Flash::success(__('Dialog successfully removed'));
        $this->redirect( $this->request->referrer() );
    }

    /**
     * Archive dialog
     */
    public function action_pack(){
        $dialog = $this->_getCurrentDialog();
        $dialog->archive( $this->current_user->id );
        Flash::success(__('Dialog successfully archived'));
        $this->redirect( $this->request->referrer() );
    }

    /**
     * Archive dialog
     */
    public function action_unpack(){
        $dialog = $this->_getCurrentDialog();
        $dialog->unarchive( $this->current_user->id );
        Flash::success(__('Dialog successfully unarchived'));
        $this->redirect( $this->request->referrer() );
    }

    /**
     * Gets current requested dialog
     * or throws 404 error page
     * @return ORM
     * @throws HTTP_Exception_404
     * @throws Kohana_Exception
     */
    protected function _getCurrentDialog(){
        $id = Request::current()->param('id');
        $dialog = ORM::factory('UserDialog')
            ->where_open()
            ->where('user_id', '=', $this->current_user->id)
            ->or_where('opponent_id', '=', $this->current_user->id)
            ->where_close()
            ->and_where('id', '=', $id)
            ->find();
        if(!$dialog->loaded())
            throw new HTTP_Exception_404('Диалог не найден');
        return $dialog;
    }
}