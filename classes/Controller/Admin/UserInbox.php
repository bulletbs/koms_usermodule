<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 * User: butch
 */
class Controller_Admin_UserInbox extends Controller_Admin_Crud
{
    protected $_model_name = 'UserDialog';
    protected $_item_name = 'dialog';

    public $skip_auto_content_apply = array(
        'dialog',
        'delete',
    );

    protected $list_fields = array(
        'id',
        'subject',
        'user_name',
        'opponent_name',
        'cnt',
    );


    public function before(){
        parent::before();
        if(empty($this->_model_name))
            throw new Kohana_Exception('There is no model to moderate');

        $route_params = array(
            'controller'=>lcfirst($this->request->controller()),
            'id'=>NULL,
        );

        $this->_user_uri = 'admin/users';
        $this->_crud_uri = 'admin/userInbox';

        /* getting sort field and direction */
        $this->_orderby_field = Arr::get($_GET, 'orderby', $this->_orderby_field);
        $this->_orderby_direction= Arr::get($_GET, 'orderdir', $this->_orderby_direction);
    }

    /**
     * List action
     * @throws Kohana_Exception
     */
    public function action_index(){
        $this->scripts[] = "media/libs/bootstrap/js/bootbox.min.js";
        $this->scripts[] = "media/libs/bootstrap/js/bbox_".I18n::$lang.".js";
        $this->scripts[] = "media/js/admin/check_all.js";

        $orm = ORM::factory('UserDialog');
        $count = $orm->count_all();
        $pagination = Pagination::factory(
            array(
                'total_items' => $count,
                'group' => 'admin_float',
            )
        )->route_params(
            array(
                'controller' => Request::current()->controller(),
            )
        );
        /**
         * @var $comment ORM
         */
        $orm = ORM::factory('UserDialog')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by($this->_orderby_field, $this->_orderby_direction);
        $items = $orm->find_all();
        $this->template->content = View::factory('admin/inbox/index')
            ->set('pagination', $pagination)
            ->set('items', $items)

            ->set('crud_uri',$this->_crud_uri)
            ->set('user_uri',$this->_user_uri)
            ->set('item_name',$this->_item_name)
            ->set('labels',$this->_getModelLabels())
        ;
    }

    /**
     * Show dialog action
     */
    public function action_dialog(){
        $id = Request::current()->param('id');
        $dialog = ORM::factory('UserDialog', $id);
        if($dialog->loaded()){
            $this->template->content = View::factory('admin/inbox/dialog')->set(array(
                'dialog' => $dialog,
                'messages' => $dialog->messages->find_all()
            ));
        }
        else{
            Flash::error('Dialog not found');
            $this->redirect( Request::current()->referrer() );
        }
    }

    /**
     * Show dialog action
     */
    public function action_delete(){
        $id = Request::current()->param('id');
        $dialog = ORM::factory('UserDialog', $id);
        if($dialog->loaded()){
            $dialog->delete();
            Flash::error(__('Dialog successfully removed'));
        }
        else{
            Flash::error('Dialog not found');
        }
        $this->redirect( Request::current()->referrer() );
    }

    /**
     * Multi action related to button
     */
    public function action_multi(){
        $ids = Arr::get($_POST, 'operate');
        if(isset($_POST['delete_all']) && count($ids)){
                $this->_delSelected($ids);
            Flash::success(__('All items (:count) was successfully deleted', array(':count'=>count($ids))));
        }
        $this->redirect( Request::current()->referrer() );

    }

    /**
     * Delete all selected comment
     * @param array $ids
     * @return object
     */
    protected function _delSelected(Array $ids){
        $count = ORM::factory($this->_model_name)->where('id','IN',$ids)->count_all();
        $items = ORM::factory($this->_model_name)->where('id','IN',$ids)->find_all();
        foreach($items as $item)
            $item->delete();
        return $count;
    }
}
