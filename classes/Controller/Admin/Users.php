<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 * User: butch
 * Date: 23.05.12
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
class Controller_Admin_Users extends Controller_Admin_Crud
{
    public $submenu = 'adminUserMenu';

    public $skip_auto_render = array(
        'delete',
        'status',
        'import',
        'force',
        'userin',
    );

    protected $_item_name = 'user';
    protected $_crud_name = 'Users';

    protected $_model_name = 'User';

    public $list_fields = array(
        'id',
        'email',
    );

    protected $_filter_fields = array(
        'id' => array(
            'label' => 'ID',
            'type'=>'digit',
        ),
        'email' => array(
            'label' => 'Email',
            'type'=>'text',
            'oper'=>'like',
        ),
    );


    protected $_form_fields = array(
        'username' => array('type'=>'text'),
        'email' => array('type'=>'text'),
        'password' => array('type'=>'password'),
        'password_confirm' => array('type'=>'password'),

        'roles' => array(
            'type'=>'call_view',
            'data'=>'admin/users/roles',
            'advanced_data'=>array(
                'roles'=>array(),
            )
        ),
        'profile_info' => array('type'=>'legend', 'name'=>'User profile'),
        'profile_block' => array(
            'type'=>'call_view',
            'data'=>'admin/users/profile',
            'advanced_data'=>array(
                'profile' => array(),
            )
        ),
    );

    protected $_advanced_list_actions = array(
        array(
            'action' => 'userin',
            'label' => 'Login as user',
            'icon' => 'log-in',
        ),
    );


    public function action_index(){
//        $this->_filter_fields['email']['data'] = $this->_filter_values['email'];

        parent::action_index();
    }

    /**
     * Delete item
     */
    public function action_delete(){
//        $user = ORM::factory('user', $this->request->param('id'));
        parent::action_delete();
    }

    /**
     * Form render/process method
     * @param $model Model_User
     * @param $data
     * @return array|bool
     */
    protected function _processForm($model, $data = array()){
        $this->_form_fields['roles']['advanced_data']['roles'] = ORM::factory('Role')->find_all();
        $this->_form_fields['roles']['advanced_data']['model_roles'] = $model->roles->find_all();
        $this->_form_fields['profile_block']['advanced_data']['profile'] = $model->profile;

        parent::_processForm($model);
    }

    /**
     * Saving Model Method
     * @param $model
     * @throws ORM_Validation_Exception
     */
    protected function _saveModel($model){
        /* Process POST */
        if(isset($_POST['cancel'])){
            $this->go("admin/users");
        }
        if(isset($_POST['submit'])){
            $errors = array();
            $userdata = Arr::extract($_POST, array('username', 'email', 'password', 'password_confirm', 'roles'));
            if(empty($userdata['password'])){
                unset($userdata['password']);
                unset($userdata['password_confirm']);
            }
            $profiledata = Arr::extract($_POST, array('name'));

            /* VALIDATE FORM DATA*/
            $validate = clone($model);
            $validate->values($userdata);
            $validate->profile->values($profiledata);
            $error = new ORM_Validation_Exception('', Validation::factory(array()));
            try{
                $extra = Model_User::get_password_validation($userdata);
                $validate->check($extra);
            }
            catch(ORM_Validation_Exception $e){
                $error->merge($e);
            }
            try{
                $validate->profile->check();
            }
            catch(ORM_Validation_Exception $e){
                $error->merge($e);
            }
            foreach($error->errors('validation') as $_errors)
                $errors = Arr::merge($errors, $_errors);

            /* SAVE FORM DATA */
            if(!count($errors)){
                if($model->loaded()){
                    $model->update_user($userdata);
                    $model->remove('roles');
                }
                else
                    $model->create_user($userdata, array('username', 'password', 'email', ));

                if(is_array($userdata['roles']))
                    $model->add('roles', $userdata['roles']);

                if(!$model->profile->loaded())
                    $model->profile->user_id = $model->pk();
                $model->profile->values($profiledata);
                $model->profile->save();
            }
            else{
                $model->values($_POST);
                $model->profile->values($_POST);
                throw $error;
            }
        }
    }

    public function action_import(){
        set_time_limit(600);
        $users = DB::select()->from('jos_users')
            ->as_assoc()
            ->execute();

        foreach($users as $user){
            $user['username'] = substr($user['username'], 0, 32);
            $model = ORM::factory('User')->where('email', '=', $user['email'])->or_where('username', '=', $user['username'])->find();
            $count = DB::select(array(DB::expr('count(*)'), 'cnt'))->from('catalog_company')->where('user_id', '=', $user['id'])->as_assoc()->execute();
            if(!$model->loaded() && Valid::email($user['email']) && $count[0]['cnt']){
                $model = ORM::factory('User');
                list($model->password) = explode(':', $user['password']);
                try{
                    $user['password'] = md5($user['password']);
                    $user['password_confirm'] = $user['password'];
                    $model->create_user($user, array('id', 'username', 'password', 'email', ));

                    $model->profile->user_id = $model->id;
                    $model->profile->name = $user['name'];
                    $model->profile->save();
                }
                catch(ORM_Validation_Exception $e){
                    echo Debug::vars($e->errors('validation'));
                    $objects = $e->objects();
                    echo Debug::vars($objects['_object']->data());
                    die();
                }
            }
            elseif($model->loaded() && $count[0]['cnt']>0){
                echo Debug::vars($user);
            }

        }
    }

    public function action_force(){
        $id = $this->request->param('id');
        if($id){
            $user = ORM::factory('User', $id);
            if($user->loaded())
                Auth::instance()->force_login($user);
        }
        $this->redirect('admin');
    }

    /**
     * Logging admin as simple user
     */
    public function action_userin(){
        $id = Request::current()->param('id');
        $user = ORM::factory('User', $id);
        if($user->loaded()){
            Session::instance()->set(Model_User::SESSION_SUBUSER_NAME, $user->id);
            $this->redirect('/my-ads');
        }
        $this->redirect( Request::current()->referrer() );
    }
}
