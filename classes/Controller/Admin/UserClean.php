<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 * User: butch
 * Date: 12.07.17
 * Time: 00:22
 * To change this template use File | Settings | File Templates.
 */
class Controller_Admin_UserClean extends Controller_System_Admin
{
    public $skip_auto_content_apply = array(
        'cleanNoAds',
    );

    public function action_index(){
        $counts = array();

        // select count(*) from users u WHERE u.id NOT IN (select DISTINCT(user_id) from ads)
        $sql = DB::select(DB::expr('count(*) cnt'))->from(array('users', 'u'))->where('logins','=',0)->and_where('u.id','NOT IN',DB::expr('(select DISTINCT(user_id) from ads)'));
        $cnt = $sql->execute();
        $counts['no_ads'] = $cnt[0]['cnt'];

//        $sql = DB::select(DB::expr('count(*) cnt'))->from(array('users', 'u'))->where('logins','=',0);
//        $cnt = $sql->execute();
//        $counts['no_auths'] = $cnt[0]['cnt'];

        $this->template->content->set(array(
            'counts' => $counts,
        ));
    }

    public function action_cleanNoAds(){
        $sql = DB::select(DB::expr('count(*) cnt'))->from(array('users', 'u'))->where('logins','=',0)->and_where('u.id','NOT IN',DB::expr('(select DISTINCT(user_id) from ads)'));
        $cnt = $sql->execute();
        $cnt = $cnt[0]['cnt'];
        $limit = 1000;
        $steps = ceil($cnt / $limit);
        if($limit*$steps > 5000)
            $steps = ceil(5000/$limit);

        for($step=0; $step < $steps; ++$step){
            $sql = DB::select()->from(array('users', 'u'))->where('logins','=',0)->and_where('u.id','NOT IN',DB::expr('(select DISTINCT(user_id) from ads)'))->limit($limit);
            $users = $sql->as_object('Model_User')->execute();
            /**
             * @var $user Model_User
             */
            foreach($users as $user){
                $user->delete();
                unset($user);
            }
            unset($users, $sql);
        }
        Flash::success("Успешно удалены ".($limit*$steps)." пользователей");
        $this->redirect(Request::initial()->referrer());
    }
}
