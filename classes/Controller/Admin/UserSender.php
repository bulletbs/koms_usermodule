<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 * User: butch
 * Date: 23.05.12
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
class Controller_Admin_UserSender extends Controller_System_Admin
{
    public $skip_auto_content_apply = array(
        'create',
        'send',
        'sendtest',
    );

    public function action_index(){
        $this->scripts[] = "media/libs/bootstrap/js/bootbox.min.js";
        $this->scripts[] = "media/libs/bootstrap/js/bbox_".I18n::$lang.".js";

        $config = Kohana::$config->load('users');
        $total = ORM::factory('UserMailer')->count_all();
        $last = ORM::factory('UserMailer')->where('sended', '=', 0)->count_all();

        /* Find text files */
        $file = Kohana::find_file('data', 'mailtext', 'txt');
        $subjfile = Kohana::find_file('data', 'mailsubj', 'txt');

        /* Save texts */
        if(Request::current()->method() == Request::POST){
            $text = Arr::get($_POST, 'mailtext');
            file_put_contents($file, $text);
            $text = Arr::get($_POST, 'mailsubj');
            file_put_contents($subjfile, $text);
        }

        /* load text data */
        $text = '';
        if(is_file($file))
            $text = file_get_contents($file);
        $subject = '';
        if(is_file($subjfile))
            $subject = file_get_contents($subjfile);
//
        $this->template->content->set(array(
            'total' => $total,
            'last' => $last,
            'stepcount' => $config->mails_per_queue,
            'mailtext' => $text,
            'mailsubj' => $subject,
            'admin_email' => $this->config['contact_email'],
        ));
    }

    /**
     * Create lists of email
     * @throws Kohana_Exception
     */
    public function action_create(){
        $table = ORM::factory('UserMailer')->table_name();
        DB::delete( $table )->execute();

        $perstep = 1000;
        $count = ORM::factory('User')->where('email_verified', '=', 1)->count_all();
        $steps = ceil($count / $perstep);

        for($i=0; $i < $steps; $i++){
            $users = DB::select('id', 'email')->from( ORM::factory('User')->table_name() )
                ->where('email_verified', '=', 1)
                ->and_where('no_mails', '=', 0)
                ->limit($perstep)
                ->offset($i * $perstep)
                ->as_assoc()
                ->execute();
            $query = DB::insert($table, array('id', 'email'));
            foreach($users as $user){
                $query->values(array(
                    $user['id'],
                    $user['email'],
                ));
            }
            $query->execute();
        }
        Flash::success('Очередь рассылки создана');
        $this->go('/admin/userSender');
    }

    /**
     * Send another pack of letters
     * @throws Kohana_Exception
     */
    public function action_send(){
        $config = Kohana::$config->load('users');
        $mails = ORM::factory('UserMailer')
            ->where('sended', '=', 0)
            ->limit($config->mails_per_queue)
            ->find_all();

        foreach($mails as $step=>$mail){
            if($step >= $config->mails_per_queue)
                break;
            $this->_sendMailerLetter($mail->email, $mail->id);
            $mail->sended = 1;
            $mail->update();
        }
        Flash::success('Отправлено писем: '. $config->mails_per_queue);
        $this->go('/admin/userSender');
    }

    /**
     * Test letter sending
     */
    public function action_sendtest(){
        $mail = $this->config['contact_email'];
        $this->_sendMailerLetter($mail, $this->current_user->id);
        Flash::success('Тестовое письмо отправлено на '. $mail);
        $this->go('/admin/userSender');
    }

    /**
     * Sending one letter to email
     * @param $mail
     * @throws Kohana_Exception
     * @throws View_Exception
     */
    protected function _sendMailerLetter($mail, $user_id){
        /* Find text files */
        $file = Kohana::find_file('data', 'mailtext', 'txt');
        $subjfile = Kohana::find_file('data', 'mailsubj', 'txt');

        /* load text data */
        $text = '';
        if(is_file($file))
            $text = file_get_contents($file);
        $subject = '';
        if(is_file($subjfile))
            $subject = file_get_contents($subjfile);

        /* Get template */
        $template = View::factory('admin/userSender/letter')->set(array(
            'site_name'=> $this->config['project']['name'],
            'server_name'=> $_SERVER['HTTP_HOST'],
            'text'=> $text,
            'unsubscribe_link' => Model_User::generateCryptoLink('unsubscribe', $user_id),
        ));

        Email::instance()
            ->reset()
            ->to($mail)
            ->from($this->config['robot_email'])
            ->subject($this->config['project']['name'] .': '.$subject)
            ->message($template->render(), true)->send();

    }
}
