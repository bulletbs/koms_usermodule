<?php
/**
 * Created by JetBrains PhpStorm.
 * User: bullet
 * Date: 06.05.12
 * Time: 8:13
 * To change this template use File | Settings | File Templates.
 */

class Controller_UserCrypt extends Controller_System_Page
{
    public $content_template = 'global/profile_layout';

    protected $cryptuser;

    public $skip_auto_content_apply = array(
        'crypted_action',
    );

    /**
     * Apply unauthrized user actions by crypted link
     * Parameters list:
     * 0 - action (unsubscribe, confirm, etc)
     * 1 - user_id
     * 2 - parameters (serialized)
     * 3 - generation timestamp
     */
    public function action_crypted_action(){
        /* Getting action data */
        $value = Request::current()->param('crypt');
        $value = Model_User::getCryptoData($value);
        if($value === FALSE)
            throw new HTTP_Exception_404();
        if($value[3] < time() - Date::DAY * 14)
            throw new HTTP_Exception_410();

        /* Loading user */
        $this->cryptuser = ORM::factory('User', $value[1]);
        if(!$this->cryptuser->loaded())
            throw new HTTP_Exception_404();
        $this->cryptuser->load_roles();

        /* Banned users go home */
        if($this->cryptuser->has_role('banned'))
            throw new HTTP_Exception_403();

        /* Run method or throw 404 */
        if(method_exists($this, '_'.$value[0]))
            $this->{'_'.$value[0]}($value);
        else
            throw new HTTP_Exception_404();

    }

    /**
     * Unsubscribe user from mailing list
     * @param $value
     * @throws Kohana_Exception
     */
    protected function _unsubscribe($value){
        $this->cryptuser->set('no_mails', 1)->update();
        if(!Auth::instance()->logged_in())
            Auth::instance()->force_login($this->cryptuser);
        Flash::success(__('You successfully unsubscribed'));
        $this->redirect( Route::get('auth')->uri(array('action'=>'profile')) );
    }

    /**
     * Handle link from email message
     * redirects auth user to dialog page
     * @param $value
     * @throws Kohana_Exception
     */
    protected function _messaging($value){
        $this->__verifyUser();
        if(!Auth::instance()->logged_in())
            Auth::instance()->force_login($this->cryptuser);
        $this->redirect( Route::get('messaging')->uri(array(
            'action'=>'dialog',
            'id'=>$value[2]['dialog_id'],
        )) .'#form' );
    }


    /**
     * Confirm email of newly registered user
     * @param $value
     * @throws Kohana_Exception
     */
    protected function _confirmation($value){
        $this->__verifyUser();
        Auth::instance()->force_login($this->cryptuser);
        $this->redirect( Route::get('auth')->uri(array('action'=>'profile')) );
    }

    /**
     * Authenticate user and redirect to change password page
     * @param $value
     * @throws HTTP_Exception_403
     * @throws Kohana_Exception
     */
    protected function _change_password($value){
        $this->__verifyUser();
        Auth::instance()->force_login($this->cryptuser);
        $this->redirect( Route::get('auth')->uri(array('action'=>'password')) );
    }

    /**
     * Changes user email
     * @param $value
     * @throws HTTP_Exception_403
     * @throws Kohana_Exception
     */
    protected function _change_mail($value){
        try{
            $this->cryptuser->values(array(
                'email' => $value[2]['email'],
                'username' => $value[2]['email'],
            ))->update();
            Flash::success(__('Email changed successfully'));
        }
        catch(ORM_Validation_Exception $e){
            $errors = '<b>'. __('An error occurred') .'</b>';
            foreach($e->errors('messages/validation') as $_err)
                $errors .= "<br> - ". $_err;
            Flash::error($errors);
        }
        Auth::instance()->force_login($this->cryptuser);
        $this->redirect( Route::get('auth')->uri(array('action'=>'profile')) );
    }

    /**
     * Verify user by link from email
     * @throws Kohana_Exception
     */
    protected function __verifyUser(){
        if(!$this->cryptuser->has_role('login')){
            $role = ORM::factory('Role')->where('name', '=', 'login')->find();
            $this->cryptuser->add('roles', $role);
            Flash::success(__('Your registration successfully finished'));
        }
        if(!$this->cryptuser->email_verified){
            $this->cryptuser->email_verified = 1;
        }

        try{
            $this->cryptuser->update();
        }
        catch(ORM_Validation_Exception $e){
            $errors = '<b>'. __('An error occurred') .'</b>';
            foreach($e->errors('messages/validation') as $_err)
                $errors .= "<br> - ". $_err;
            Flash::error($errors);
        }
    }
}