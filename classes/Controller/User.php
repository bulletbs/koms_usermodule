<?php
/**
 * Created by JetBrains PhpStorm.
 * User: bullet
 * Date: 06.05.12
 * Time: 8:13
 * To change this template use File | Settings | File Templates.
 */

class Controller_User extends Controller_System_Page
{
    public $user_content;
    public $right_menu;
    public $right_contents;
    public $content_template = 'global/profile_layout';

    public $auth_required = 'login';

    public $secure_actions = array(
        'enter' => NULL,
        'register' => NULL,
        'registration_done' => NULL,
        'confirm' => NULL,
        'confirmation_needed' => NULL,
        'resend' => NULL,
        'changepass' => NULL,
        'changepass_reset' => NULL,
        'ajax_cities' => NULL,
        'crypted_action' => NULL,
    );

    public $skip_auto_content_apply = array(
        'logout',
        'delete_social',
        'ajax_cities',
        'crypted_action',
    );


    public function before(){
        parent::before();

        /* Хлебные крошки */
        $this->breadcrumbs->add('Личный кабинет', '/profile');

        if(!in_array($this->request->action(), $this->skip_auto_content_apply))
            $this->user_content = $this->getContentTemplate();
        $this->styles[] = 'media/libs/pure-release-0.6.0/forms-min.css';
        $this->styles[] = 'media/libs/pure-release-0.6.0/grids-min.css';
        $this->styles[] = 'media/libs/pure-release-0.6.0/tables-min.css';
        $this->styles[] = 'media/libs/pure-release-0.6.0/menus-min.css';
    }

    public function after(){
        if(!Request::$current->is_ajax()){
            if($this->logged_in){
                $menu = $this->getContentTemplate('user/right_menu')
                    ->set(array(
                        'modules' => Kohana::modules(),
                        'more_contents' => $this->right_contents,
                    ));
                $this->template->content->set('right_column', $menu);
                $this->template->content->profile_content = $this->user_content;
            }
            else{
                $this->template->content = $this->user_content;
            }
        }
        parent::after();
    }

    /**
     * USER PROFILE
     * @return void
     */
    public function action_profile()
    {
        $errors = array();
        $user = $this->current_user;

        if($this->logged_in && $this->request->method()=='POST' && Arr::get($_POST, 'update')) {
            $userdata = Arr::extract($_POST, array('username', 'no_mails')); //'email',

            /* Profile data */
            $profiledata = Arr::extract($_POST, array('name', 'address', 'phone', 'city_id'));
            try{
                if($user->loaded()){
                    $user->update_user($userdata);
                    $user->profile->values($profiledata);
                    $user->profile->save();
                    if(isset($_FILES['avatar']))
                        $user->profile->saveAvatar($_FILES['avatar']['tmp_name']);
                    Flash::success(__('Profile was successfully updated'));
                    $this->go('profile');
                }

            }catch(ORM_Validation_Exception $e){
                $errors = $e->errors('validation');
            }
        }

        /* Ulogin init */
        if(Ulogin::factory()->mode()){
            try
            {
                $user = Ulogin::factory()->login();
                if(Auth::instance()->logged_in())
                    /* Добавлено успешно */
                    if(count($user) && Auth::instance()->logged_in('login')){
                        Flash::success(__('Social network account successfully added'));
                        $this->go('profile');
                    }
                    /* Принадлежит другому пользователю */
                    else{
                        Flash::error(__('This social account is already added by another user'));
                        $this->go('profile');
                    }
                else{

                }
            }
            catch(ORM_Validation_Exception $e)
            {
                $errors = $e->errors('');
            }
            catch(HTTP_Exception_403 $e)
            {
                $errors = array($e->getMessage());
            }
        }


        /* Регионы и города */
        $regions = array(''=>"Выберите регион");
        $regions += ORM::factory('BoardCity')->where('parent_id', '=', 0)->cached(Model_BoardCity::CITIES_CACHE_TIME)->find_all()->as_array('id','name');
        $cities = '';
        $region = NULL;
        if($user->profile->city_id > 0){
            $city = ORM::factory('BoardCity', $user->profile->city_id);
            $region = $city->parent_id;
            $cities = $this->_render_city_list($city->parent(), $user->profile->city_id);
        }
        else{
            if(NULL !== $region = Arr::get($_POST, 'region')){
                $cities = $this->_render_city_list(ORM::factory('BoardCity', $region));
            }
        }

        $ulogin = Ulogin::factory()->render();
        $photo = '/media/css/images/no-avatar.jpg';

        if (is_file(DOCROOT.'/media/upload/profiles/'.$this->current_user->id . ".jpg"))
            $photo = "/media/uploads/users/" . $this->current_user->id . ".jpg";
        if(Auth::instance()->logged_in())
            $networks = DB::select("id", "identity", "network")->from('ulogins')->where("user_id", "=", Auth::instance()->get_user()->id)->execute()->as_array();
        if($this->is_mobile){
            $this->mobile_scripts[] = "assets/user/js/profile.js";
        }
        else{
            $this->scripts[] = "assets/user/js/profile.js";
            $this->scripts[] = "media/libs/jquery-form-styler/jquery.formstyler.min.js";
            $this->styles[] = "media/libs/jquery-form-styler/jquery.formstyler.css";
        }

        $this->user_content->set(array(
            'ulogin' => $ulogin,
            'networks' => $networks,
            'photo' => $photo,
            'user' => $user,
            'profile' => $user->profile,
            'errors' => $errors,

            'regions' => $regions,
            'region' => $region,
            'cities' => $cities,
        ));
    }

    /**
     * SIGNIN & REGISTRATION
     */
    public function action_enter(){
        /* Если авторизирован - переходим в профиль */
        if(Auth::instance()->logged_in('login'))
            $this->go('profile');

        /* Если авторизирован без прав - выходим и обновляем страницу */
        if(Auth::instance()->logged_in()){
            Auth::instance()->logout();
            $this->go('enter');
        }

        /* Native login & register */
        $posted = Arr::extract($_POST, array('login', 'register'));
        if($posted['login']){
            $login_data = $_POST;
            $login_errors = $this->_login();
        }

        /* Ulogin login & register */
        $ulogin = Ulogin::factory();
        if ($ulogin->mode())
        {
            try
            {
                $_user_data = $ulogin->login();
                if(Auth::instance()->logged_in())
                    /* LOgin successfull */
                    if(Auth::instance()->logged_in('login'))
                        $this->go(URL::site(Route::get('board_myads')->uri(), KoMS::protocol()));
                    /* Registration confirm needed message */
                    else{
                        $this->go('user/confirmation_needed');
                    }
                else{
                    $this->_sendConfimationEmail(ORM::factory('User', $_user_data['user_id']));
                    $this->go('user/registration_done');
                }
            }
            catch(ORM_Validation_Exception $e)
            {
                $ulogin_errors = $e->errors('');
            }
            catch(Kohana_HTTP_Exception_403 $e) {
                $ulogin_errors[] = $e->getMessage();
            }
            catch(Kohana_HTTP_Exception_302 $e) {
                throw $e;
            }
            catch(Kohana_Exception $e) {
                $ulogin_errors[] = __('An error occured while sign in by social network. Try to sing in by your login and password');
            }
        }

        $login_form = $this->getContentTemplate('user/login')->bind('errors', $login_errors)->bind('data', $login_data);
        $register_text = $this->getContentTemplate('user/registration_text');
        $social_buttons = $this->getContentTemplate('user/ulogin')->set('buttons',  $ulogin->render())->bind('errors', $ulogin_errors);

        $this->user_content->set(array(
            'login_form' => $login_form,
            'register_text' => $register_text,
            'social_buttons' => $social_buttons,
        ));
    }

    /**
     * Удаление привязки социальной сети к аккаунту
     */
    public function action_delete_social(){
        $id = Request::initial()->param('id');
        if($id){
            $deleted = DB::delete("ulogins")->where("user_id", "=", Auth::instance()->get_user()->id)->and_where('id', '=', $id)->execute();
            if($deleted)
                Flash::success(__('Social network account successfully deleted'));
            else
                Flash::error(__('Social network account not found'));
        }
        $this->go('profile');
    }

    /**
     * Выход пользователя
     * @return void
     */
    public function action_logout()
    {
        $this->auto_render = FALSE;
        if (Auth::instance()->logged_in()) {
            if(!is_null($subuser = Session::instance()->get(Model_User::SESSION_SUBUSER_NAME))){
                Session::instance()->delete(Model_User::SESSION_SUBUSER_NAME);
                $this->redirect(Route::get('admin')->uri(array('controller'=>'board')));
            }
            else
                Auth::instance()->logout();
        }
        $this->redirect('/');
    }

    /**
     * Смена пароля пользователя
     */
    public function action_password(){
        if($this->request->method()=='POST'){
            $errors = array();
            $validation = Validation::factory($_POST);
            $validation->labels($this->current_user->labels());
            $validation->rule('password','not_empty');
            if(!$validation->check())
                $errors = $validation->errors('validation');
            else{
                try{
                    $this->current_user->update_user($_POST,array('password'));
                    Flash::success(__("Your password successfully changed"));
                    $this->go(Request::$initial->url());
                }
                catch(ORM_Validation_Exception $e){
                    $errors = $e->errors('validation');
                }
            }
        }
        $this->user_content->bind('errors', $errors);
    }

    /**
     * User registration action
     */
    public function action_register(){
        if(Auth::instance()->logged_in() && !$this->request->is_ajax())
            $this->go('profile');
        if(Request::initial()->method() == Request::POST){
            $data = Arr::extract($_POST, array('name', 'username', 'password', 'password_confirm', 'email', 'captcha'));
            $errors = $this->_validateUserData($data);
            if(!count($errors)){
                try{
                    $this->_saveUserData($data);
                    $this->go('user/registration_done');
                }
                catch(ORM_Validation_Exception $e){
                    $errors = $e->errors('validation');
                }
            }
        }
        $this->user_content->bind('errors', $errors);
        $this->user_content->bind('data', $data);
    }

    /**
     * Save user data array as New User
     * @param $data
     * @return mixed
     */
    protected function _saveUserData($data){
        $user = ORM::factory('User')->create_user($data, array('username', 'password', 'email'));
        $user->profile->user_id = $user->id;
        $user->profile->values($data);
        $user->profile->save();
        $this->_sendConfimationEmail($user);
        return $user;
    }

    /**
     * Validate user data array
     * @param $data
     * @return array
     */
    protected function _validateUserData($data){
        $errors = array();

        $validate = ORM::factory('User')->values($data);
        $validate->profile->values($data);
        $validate->profile->id='-1';
        $error = new ORM_Validation_Exception('', Validation::factory(array()));
        try{
            $extra = Model_User::get_password_validation($data);
            $extra->rules('g-recaptcha-response', array(
                array('Captcha::check', array(':value', ':validation', ':field'))
            ));
            $validate->check($extra);
        }
        catch(ORM_Validation_Exception $e){
            $error->merge($e);
        }
        try{
            $validate->profile->check();
        }
        catch(ORM_Validation_Exception $e){
            $error->merge($e);
        }
        foreach($error->errors('validation') as $_errors)
            $errors = Arr::merge($errors, $_errors);

        return $errors;
    }

    /**
     * Сообщение о том, что регистрация окончена
     */
    public function action_registration_done(){
        $this->user_content->set(array(
            'site_name' => $this->config['project']['name'],
        ));
    }

    /**
     * ОБработка ссылки с подтверждением регистрации
     */
    public function action_confirm(){
        $id = Request::initial()->param('id');
        $hash = Request::initial()->param('hash');
        $user = ORM::factory('User', $id);
        if($id && $hash && $user && md5($user->id.$user->email.$user->password) == $hash){
            $role = ORM::factory('Role')->where('name', '=', 'login')->find();
            $user->add('roles', $role);
            $user->email_verified = 1;
            $user->update();
            Auth::instance()->force_login($user);
            Flash::success(__('Your registration successfully finished').'!');
            $this->go('profile');
        }
    }

    /**
     * Сообщение о том, что регистрация не завершена
     * и кнопкой для повторной отправки письма с подтверждением
     */
    public function action_confirmation_needed(){
        if(Auth::instance()->logged_in()){
            $this->user_content->set(array(
                'user' => $this->current_user,
            ));
            Auth::instance()->logout();
        }
        else
            $this->go('enter');
    }

    /**
     * Повторная отправка письма с подтверждением регистрации
     */
    public function action_resend(){
        $sended = false;
        $id = Request::initial()->param('id');
        $user = ORM::factory('User', $id);
        if($user->loaded() && !$user->email_verified){
            $this->_sendConfimationEmail($user);
            $sended = true;
        }
        $this->user_content->set(array(
            'sended' => $sended,
        ));
    }

    /**
     * Запрос о смене пароля
     */
    public function action_change_mail(){
        $errors = array();
        $sended = false;

        if($this->request->method() == Request::POST){
            $data = Arr::extract($_POST, array('email', 'email_confirm'));

            $validation = Validation::factory($data)
                ->rules('email', array(
                    array('not_empty'),
                    array('email'),
                    array('matches', array(':validation', 'email_confirm', ':field')),
                    array(array($this->current_user, 'unique'), array('email', ':value')),
                ))
                ->labels(array(
                    'email' => __('New email'),
                    'email_confirm' => __('Email confirm'),
                ));

            try{
                if(!$validation->check())
                    throw new Validation_Exception($validation);
                $link = Model_User::generateCryptoLink('change_mail', $this->current_user->id, array('email' => $data['email']));
                $message = View::factory('user/changemail_letter')->set(array(
                    'user'=>$this->current_user->profile->name,
                    'site_name'=> $this->config['project']['name'],
                    'server_name'=> $_SERVER['HTTP_HOST'],
                    'changemail_link'=> $link,
                ))->render();
//                        file_put_contents(DOCROOT. '/debug_mail.txt', PHP_EOL.PHP_EOL. $message, FILE_APPEND);
                Email::instance()
                    ->to($data['email'])
                    ->from($this->config['robot_email'])
                    ->subject(__('Changing the email') .' '. $this->config['project']['name'])
                    ->message($message, true)
                    ->send();
                $this->user_content = View::factory('user/change_mail_sended');
                return;
            }
            catch(Validation_Exception $e){
                $errors = $validation->errors('error/validation');
            }
            catch(ORM_Validation_Exception $e){
                $errors = $validation->errors('error/validation');
            }
        }

        $this->user_content->set(array(
            'sended' => $sended,
            'errors' => $errors,
        ));
    }

    /**
     * Запрос о смене пароля
     */
    public function action_changepass(){
        $errors = array();
        $sended = false;
        $data = array(
            'username' => Arr::get($_POST, 'username'),
        );
        if($data['username'] && Valid::email($data['username'])){
            $user = ORM::factory('User')->where('username', '=', $data['username'])->find();
            if($user->loaded()){
                $this->_sendChangePasswordEmail($user);
                $sended = true;
            }
            else
                $errors[] = __('A user with this e-mail was not found');
        }
        $this->user_content->set('sended', $sended);
        $this->user_content->set('errors', $errors);
        $this->user_content->set('data', $data);
    }

    /**
     * Смена пароля пользователя по ссылке из письма
     */
    public function action_changepass_reset(){
        $id = Request::initial()->param('id');
        $hash = Request::initial()->param('hash');
        $user = ORM::factory('User', $id);
        if($id && $hash && $user && md5($user->password.$user->email.$user->id) == $hash){
            $data = Arr::extract($_POST, array('password', 'password_confirm'));
            if(!empty($data['password'])){
                try{
                    $user->update_user($data, array('password'));
                    Flash::success(__('Password changed successfully, now you can sign in'));
                    $this->go('/enter');
                }
                catch(ORM_Validation_Exception $e){
                    $errors = $e->errors('validation');
                }
            }
        }
        else
            unset($user);
        $this->user_content->bind('errors', $errors);
        $this->user_content->bind('user', $user);
    }

    /**
     * Обычная авторизация на сайте
     * @return array
     */
    protected function _login(){
        /* Обработка данных формы входа */
        $data = Arr::extract($_POST, array('username', 'password', 'remember'));
        try{
            if(Auth::instance()->login($data['username'], $data['password'], $data['remember'] == 1)){
                $this->go($this->request->param('goto','/profile'));
            }
            else{
                $user = ORM::factory('User')->where('username','=',$data['username'])->and_where('password', '=', Auth::instance()->hash($data['password']))->find();
                if($user->loaded() && !$user->email_verified){
                    Auth::instance()->force_login($user);
                    $this->go('user/confirmation_needed');
                }
                else
                    $errors[] = __("Invalid user email or password");
            }
        }
        catch(ORM_Validation_Exception $e) {
            $errors[] = $e->getMessage();
        }
        catch(Kohana_HTTP_Exception_403 $e) {
            $errors[] = $e->getMessage();
        }
        if($this->request->is_ajax()) {
            $errors = array();
            $this->json = array('logged_in' => Auth::instance()->logged_in());
            if(count($errors)) $this->json['errors'] = $errors;
        }
        else
            return $errors;
    }

    /**
     * Remove user account with all of
     * @throws Kohana_Exception
     */
    public function action_remove_account(){
        if(Request::current()->method() == Request::POST){
            ORM::factory('User', $this->current_user->id)->delete();
            Auth::instance()->logout(TRUE);
            Flash::success(__('Your account successfully removed'));
            $this->user_content = View::factory('user/remove_done');
            return;
        }

        $cfg = Kohana::$config->load('global')->as_array();
        $this->user_content->set(array(
            'cfg' => $cfg,
        ));
    }

    /**
     * Get Region cities (AJAX)
     */
    public function action_ajax_cities(){
        if(!$this->request->is_ajax() && $this->request->initial())
            $this->go(Route::get('board')->uri());
        $id = $this->request->param('id');
        $region = ORM::factory('BoardCity', Arr::get($_POST, 'selectedRegion') );

        if($region->loaded()){
            $options = $region->children()->as_array('id', 'name');
            $this->json['cities'] = '';
            if(count($options))
                $this->json['cities'] = $this->_render_city_list($region);
            /* Rendering subcategories list */
            $this->json['id'] = $id;
        }
    }

    /**
     * Apply unauthrized user actions by crypted link
     * Parameters list:
     * 0 - action (unsubscribe, confirm, etc)
     * 1 - user_id
     * 2 - parameters (serialized)
     */
    public function action_crypted_action(){
        /* Getting action data */
        $value = Request::current()->param('crypt');
        $value = Model_User::getCryptoData($value);
        if($value === FALSE)
            throw new HTTP_Exception_404();

        /* Loading user */
        $user = ORM::factory('User', $value[1]);
        $user->load_roles();
        if(!$user->loaded())
            throw new HTTP_Exception_404();

        /* Applying action or throw 404 error */
        switch($value[0]){
        /* Unsubscribe user from mailing */
            case 'unsubscribe':
                $user->set('no_mails', 1)->update();
                if($user->has_role('login') && !Auth::instance()->logged_in())
                    Auth::instance()->force_login($user);
                Flash::success(__('You successfully unsubscribed'));
                $this->redirect( Route::get('auth')->uri(array('action'=>'profile')) );
                break;

        /* Goto messaging page */
            case 'messaging':
                if($user->has_role('login') && !Auth::instance()->logged_in())
                    Auth::instance()->force_login($user);
                $this->redirect( Route::get('messaging')->uri(array(
                    'action'=>'dialog',
                    'id'=>$value[2]['dialog_id'],
                )) );
                break;
            /* Unknown actions should be redirected to 404 page*/
            default:
                throw new HTTP_Exception_404();
                break;
        }

    }

    /**
     * @param ORM_MPTT $region
     * @param null $selected
     * @return bool|string
     */
    protected function _render_city_list($region, $selected = NULL){
        $options = $region->children()->as_array('id', 'name');
        if(count($options)){
            asort($options);
            $options = Arr::merge(array('' => __('Select city')), $options);
            return Form::select('city', $options, $selected, array('id'=>'city'));
        }
        return false;
    }

    /**
     * Отправляет письмо пользователю со ссылкой для подтверждения регистрации
     * @param Model_User $user
     */
    protected function _sendConfimationEmail(Model_User $user){
        $link = Model_User::generateCryptoLink('confirmation', $user->id, array());
        Email::instance()
            ->to($user->email)
            ->from($this->config['robot_email'])
            ->subject(__('Registration confirm') .' '. $this->config['project']['name'])
            ->message(View::factory('user/confirmation_letter', array(
                        'user'=>$user,
                        'site_name'=> $this->config['project']['name'],
                        'server_name'=> $_SERVER['HTTP_HOST'],
//                        'activation_link'=> 'registration_confirm/'.$user->id.'/'.md5($user->id.$user->email.$user->password),)
                        'activation_link'=> $link,)
                )->render()
                , true)
            ->send();
    }

    /**
     * Отправляет письмо пользователю со ссылкой для смены пароля
     * @param Model_User $user
     */
    protected function _sendChangePasswordEmail(Model_User $user){
        $link = Model_User::generateCryptoLink('change_password', $user->id, array());
        $content = View::factory('user/changepass_letter', array(
                'user'=>$user,
                'site_name'=> $this->config['project']['name'],
                'server_name'=> $_SERVER['HTTP_HOST'],
//                'changepass_link'=> 'reset_password/'.$user->id.'/'.md5($user->password.$user->email.$user->id),)
                'changepass_link'=> $link,
            )
        )->render();
//        file_put_contents(DOCROOT. '/debug_mail.txt', PHP_EOL.PHP_EOL. $content, FILE_APPEND);
        Email::instance()
            ->to($user->email)
            ->from($this->config['robot_email'])
            ->subject(__('Changing the password') .' '. $this->config['project']['name'])
            ->message($content, true)
            ->send();
    }
}