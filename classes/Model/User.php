<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User
{
    const SESSION_SUBUSER_NAME = 'AdminSubUserID';

    /**
     * Roles array
     * @var array
     */
    protected $_roles = array();

//    protected $_load_with = array('profile');

    protected $_has_one = array(
        'profile' => array(
            'model' => 'Profile',
            'foreign_key' => 'user_id',
        ),
        'subscriber' => array(
            'model' => 'UserMailer',
            'foreign_key' => 'user_id',
        ),
    );
    protected $_has_many = array(
        'user_tokens' => array('model' => 'User_Token'),
        'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
        'ulogins' => array('model' => 'Ulogin'),
    );

    public function rules()
    {
        return array(
//            'username' => array(
//                array('not_empty'),
//                array('max_length', array(':value', 32)),
//                array(array($this, 'unique'), array('username', ':value')),
//            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array('Model_User', 'checkBannedDomain'), array(':validation', ':value')),
                array(array($this, 'unique'), array('email', ':value')),
            ),
        );
    }

    public function labels()
    {
        return array(
            'id' => 'ID',
            'username' => __('Login'),
            'email' => 'E-mail',
            'roles' => __('Roles'),
            'profile' => __('Profile'),
            'password' => __('Password'),
            'password_confirm' => __('Password confirm'),
            'no_mails' => __('Do not send me emails'),
        );
    }

    /**
     * Automatic generation of empty values while create
     * @param array $values
     * @param array $expected
     * @return ORM
     */
    public function create_user($values, $expected)
    {
        $values['username'] = $this->generateUsername($values);
        if (!isset($values['password']) || empty($values['password']))
            $values['password'] = $values['password_confirm'] = $this->generatePassword($values);
        elseif (!isset($values['password_confirm']) || empty($values['password_confirm']) && isset($values['password']))
            $values['password_confirm'] = $values['password'];

        /* Add register timestamp */
        $values['registered'] = time();
        $expected[] = 'registered';

        return parent::create_user($values, $expected);
    }

    /**
     * Automatic generation of empty values while create
     * @param array $values
     * @param array $expected
     * @return ORM|void
     */
    public function update_user($values, $expected = NULL)
    {
//        if (isset($values['email']) && (!isset($values['username']) || empty($values['username'])) )
            $values['username'] = $this->generateUsername($values);

        parent::update_user($values, $expected);
    }

    /**
     * Automatic generation of empty values while validating model
     * @throws Validation_Exception
     */
    public function checkData()
    {
        if (empty($this->username))
            $this->username = $this->generateUsername($this->as_array());

        $validation = Validation::factory($this->as_array())
            ->bind(':model', $this)
            ->bind(':original_values', $this->_original_values)
            ->bind(':changed', $this->_changed);

        foreach ($this->rules() as $_field => $_rule)
            $validation->rules($_field, $_rule);
        foreach ($this->labels() as $_field => $_label)
            $validation->label($_field, $_label);

        if (!$validation->check()) {
            throw new ORM_Validation_Exception($this->errors_filename(), $validation);
        }
    }


    /**
     * Ручная проверка данных из POST
     * для внешней валидации данных при добавлении объявлений с созданием пользователей
     * @param array $post
     * @return array
     */
    public function validateData(Array $post){
        $valid = Validation::factory($post);
        $valid->labels($this->labels());
        foreach ($this->rules() as $field => $rules)
            $valid->rules($field, $rules);
        if(!$valid->check())
            return $valid->errors('',TRUE);
        return array();
    }

    /**
     * Password validation for plain passwords.
     *
     * @param array $values
     * @return Validation
     */
    public static function get_password_validation($values)
    {
        $validation = Validation::factory($values);
        if(isset($values['password'])){
            $validation
                ->rule('password', 'min_length', array(':value', 7))
                ->rule('password', 'not_empty')
                ->rule('password_confirm', 'not_empty')
                ->rule('password_confirm', 'matches', array(':validation', 'password', ':field'))
                ->labels(ORM::factory('User')->labels())
            ;
        }
        return $validation;
    }

    /**
     * Automatic username generation
     * @param $values
     * @return mixed
     */
    public function generateUsername($values)
    {
        if($this->loaded())
            $values['email'] = $this->email;

        return $values['email'];
    }

    /**
     * Automatic password generation
     * @param $values
     * @return string
     */
    public function generatePassword($values)
    {
        return substr(md5($values['username'] . $values['email'] . microtime()), 2, 7);
    }

    /**
     * Check if user have role
     * @param $role
     * @param bool $all_required
     * @return bool
     */
    public function has_role($role, $all_required = TRUE)
    {
        if (is_array($role)) {
            $status = (bool)$all_required;

            foreach ($role as $_role) {
                // If the user doesn't have the role
                if (!in_array($_role, $this->_roles)) {
                    // Set the status false and get outta here
                    $status = FALSE;

                    if ($all_required) {
                        break;
                    }
                } elseif (!$all_required) {
                    $status = TRUE;
                    break;
                }
            }
        } else {
            $status = in_array($role, $this->_roles);
        }

        return $status;
    }

    /**
     * User roles getter
     * @return array
     */
    public function roles()
    {
        return $this->_roles;
    }

    /**
     * Complete login saves user roles
     */
    public function complete_login()
    {
        $this->load_roles();
        if($this->has_role('banned')){
            Auth::instance()->logout();
            throw new Kohana_HTTP_Exception_403(__('Your account is banned'));
        }
        parent::complete_login();
    }

    /**
     * Load user roles
     */
    public function load_roles(){
        $this->_roles = array();
        $roles = $this->roles->find_all();
        foreach ($roles as $role) {
            $this->_roles[] = $role->name;
        }
    }


    public function serialize()
    {
        $parameters = array(
            '_primary_key_value', '_object', '_changed', '_loaded', '_saved', '_sorting',
            '_roles'
        );

        // Store only information about the object
        foreach ($parameters as $var)
        {
            $data[$var] = $this->{$var};
        }

        return serialize($data);
    }

    /**
     * Remove user ans all relations
     * @return ORM
     */
    public function delete(){
        if($this->profile->loaded())
            $this->profile->delete();
        foreach($this->ulogins->find_all() as $_ulogin)
            $_ulogin->delete();
        foreach($this->roles->find_all() as $_role)
            $this->remove('roles', $_role);
        foreach($this->user_tokens->find_all() as $_token)
            $_token->delete();

        /* Removing related models defined in 'users' config */
        $cfg = Kohana::$config->load('users')->as_array();
        if(isset($cfg['user_submodels']) && is_array($cfg['user_submodels'])){
            foreach($cfg['user_submodels'] as $submodel){
                $orm = ORM::factory($submodel['model']);
                if(is_array($submodel['foreign'])){
                    foreach($submodel['foreign'] as $_field_id=>$_field)
                        $orm->{!$_field_id ? 'where' : 'or_where'}($_field, '=', $this->id);
                }
                else
                    $orm->where($submodel['foreign'], '=', $this->id);
                $result = $orm->find_all();
                foreach($result as $row)
                    $row->delete();
            }
        }

        return parent::delete();
    }

    /**
     * Load user by email
     * @param $email
     * @return null|ORM
     * @throws Kohana_Exception
     */
    public static function loadUserByEmail($email){
        $user = ORM::factory('User')->where('email', '=', $email)->find();
        if($user->loaded())
            return $user;
        return NULL;
    }

    /**
     * Добавить пользвателя из импорта
     * @param $row
     * @return mixed
     */
    public static function import_user($row){
        /* Check if user exists */
        $user = ORM::factory('User')->where('email', '=', $row['email'])->find();
        if($user->loaded())
            return $user->id;

        $user = ORM::factory('User')->values(array(
            'email' => $row['email'],
            'username' => $row['email'],
            'password' => md5($row['email'] . time()),
            'email_verified' => $row['activ'] == 'yes',
        ))->save();
        if($user->saved()){
            $user->profile->values($row);
            $user->profile->user_id = $user->id;
            $user->profile->save();
            if(isset($row['activ']) && $row['activ'] == 'yes'){
                $user->add('roles', ORM::factory('Role', 2));
            }
            return $user->id;
        }
        return 0;
    }

    /**
     * Import user data from another KoBoard
     * @param array $data
     * @return mixed
     * @throws Kohana_Exception
     */
    public static function reimport_user(Array $data){
        /* Check if user exists */
        $user = ORM::factory('User')->where('email', '=', $data['email'])->find();
        if($user->loaded())
            return $user->id;

        $user = ORM::factory('User')->values($data)->save();
        if($user->saved()){
            $user->profile->values($data);
            $user->profile->user_id = $user->id;
            $user->profile->save();
            if(isset($data['email_verified']) && $data['email_verified'] == 1){
                $user->add('roles', ORM::factory('Role', 2));
            }
            return $user->id;
        }
        return 0;
    }

    /**
     * Generate link to make unauthorized user actions (unsubscribe, confirm, etc)
     * @param $action - action from user_controller->action_crypted_action list
     * @param $user_id - user_id for action
     * @param array $params - other action parameters
     * @return string
     * @throws Kohana_Exception
     */
    public static function generateCryptoLink($action, $user_id, Array $params = array()){
        $hash = Encrypt::instance()->encode(serialize(array(
            $action,
            $user_id,
            $params,
            time(),
        )));
        $uri = Route::get('crypt_action')->uri(array(
            'crypt' => base64_encode($hash),
        ));
        return $uri;
    }

    public static function getCryptoData($hash){
        $data = Encrypt::instance()->decode(base64_decode($hash));
        if(KoMS::isSerialized($data)){
            $data = unserialize($data);
            if(is_array($data) && count($data) == 4)
                return $data;
        }
        return false;
    }

    /**
     * Check email for banned domains
     * @param $validation
     * @param $email
     * @throws Kohana_Exception
     */
    public static function checkBannedDomain($validation, $email){
        $domains = Kohana::$config->load('users.banned_domains');
        foreach($domains as $domain)
            if(strstr($email, $domain))
                $validation->error('email', 'banned_domain');
    }
}