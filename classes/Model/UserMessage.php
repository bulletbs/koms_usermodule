<?php defined('SYSPATH') or die('No direct script access.');

class Model_UserMessage extends ORM
{
    protected $_table_name = 'user_box';

    public function labels(){
        return array(
            'id'=>'ID',
            'parent_id'=>'Ветка',

            'user_id'=>'От',
            'to_user_id'=>'Кому',

            'subject'=>'Тема сообщения',
            'message'=>'Текст сообщения',
            'sendtime'=>'Отправлено',
            'readtime'=>'Прочитано',
        );
    }

}