<?php defined('SYSPATH') or die('No direct script access.');

class Model_UserDialog extends ORM
{
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id',
        ),
        'opponent' => array(
            'model' => 'User',
            'foreign_key' => 'opponent_id',
        ),
    );

    protected $_has_many = array(
        'messages' => array(
            'model' => 'UserDialogMessage',
            'foreign_key'=>'dialog_id',
        ),
    );

    protected $_table_name = 'user_dialogs';

    public function labels(){
        return array(
            'id'=>'ID',

            'subject'=>'Тема диалога',

            'user_id'=>'От ID',
            'user_name'=>'От',

            'opponent_id'=>'Для ID',
            'opponent_name'=>'Для',

            // привязка диалога к модели на сайте
//            'subject_id'=>'ID объекта обсуждения',
//            'subject_model'=>'Тип объекта обсуждения',

            'last_message_time'=>'Время последнего',
            'last_message_user'=>'Последнее от',

            'cnt'=>'Сообщений',
        );
    }

    /**
     * Add new message in dialog
     * @param $user_id
     * @param $text
     */
    public function addMessage($user_id, $text){
        /* create and save message */
        $message = ORM::factory('UserDialogMessage');
        $message->dialog_id = $this->id;
        $message->user_id = $user_id;
        $message->text = strip_tags($text);
        $message->save();

        /* update dialog data */
        $this->last_message_time = time();
        $this->last_message_user = $user_id;
        $this->{($this->user_id == $user_id ? 'opponent' : 'user' ) . '_unread'} = 1;
        $this->{($this->user_id == $user_id ? 'opponent' : 'user' ) . '_archive'} = 0;
        $this->cnt = $this->cnt+1;
        $this->update();
    }

    /**
     * Generates dialog uri
     * @return string
     * @throws Kohana_Exception
     */
    public function getUri(){
        return Route::get('messaging')->uri(array(
            'action' => 'dialog',
            'id' => $this->id,
        ));
    }

    /**
     * Returns message text
     * @return mixed
     */
    public function getTitle(){
        return $this->subject;
    }

    /**
     * Last message date & time
     * @return bool|string
     */
    public function getLastTime(){
        return Date::smart_datetime( $this->last_message_time );
    }

    /**
     * Last message author name
     * @param null $user_id
     * @return mixed
     */
    public function getLastName($user_id = NULL){
        if($this->last_message_user == $user_id)
            $name = __('me');
        else
            $name = $this->last_message_user == $this->user_id ? $this->user_name : $this->opponent_name;
        return $name;
    }

    /**
     * Author name by userID
     * @param null $user_id
     * @return mixed
     */
    public function getUserName($user_id = NULL){
        $name = 'unknown';
        if($this->user_id == $user_id)
            $name = $this->user_name;
        elseif($this->opponent_id == $user_id)
            $name = $this->opponent_name;
        return $name;
    }

    /**
     * Delete dialog with messages
     * @return ORM|void
     */
    public function delete(){
        foreach( $this->messages->find_all() as $message)
            $message->delete();
        parent::delete();
    }

    /**
     * Move dialog to user archive
     */
    public function archive($user_id){
        if($this->user_id == $user_id)
            $this->user_archive = 1;
        elseif($this->opponent_id == $user_id)
            $this->opponent_archive = 1;
        $this->update();
    }

    /**
     * Move dialog to user archive
     */
    public function unarchive($user_id){
        if($this->user_id == $user_id)
            $this->user_archive = 0;
        elseif($this->opponent_id == $user_id)
            $this->opponent_archive = 0;
        $this->update();
    }

    /**
     * Create Dialog object
     * lookin for existing one, if none found cretaes new
     * @param int $user
     * @param string $user_name
     * @param int $opponent
     * @param string $opponent_name
     * @param int $subject_id
     * @param string $subject
     * @param string $subject_model
     * @throws Kohana_Exception
     * @return Model_UserDialog
     */
    public static function create_dialog($user, $user_name, $opponent, $opponent_name, $subject_id, $subject, $subject_model){
        $dialog = ORM::factory('UserDialog')
            ->where('user_id', '=', $user)
            ->where('opponent_id', '=', $opponent)
            ->where('subject_id', '=', $subject_id)
            ->find();
        if(!$dialog->loaded()){
            $dialog = ORM::factory('UserDialog');
            $dialog->user_id = $user;
            $dialog->user_name = $user_name;
            $dialog->opponent_id = $opponent;
            $dialog->opponent_name = $opponent_name;
            $dialog->subject = $subject;
            $dialog->subject_id = $subject_id;
            $dialog->subject_model = $subject_model;
            $dialog->save();
        }
        return $dialog;
    }

    /**
     * Check if dialog have unreaded messages for user
     * @param $user_id
     * @return bool
     */
    public function haveNewMessages($user_id){
        if($this->user_id == $user_id && $this->user_unread>0)
            return TRUE;
        if($this->opponent_id == $user_id && $this->opponent_unread>0)
            return TRUE;
        return FALSE;
    }

    /**
     * Who is
     * @param $user_id
     * @return null|string
     */
    public function userIs($user_id){
        if($this->user_id == $user_id)
            return 'user';
        if($this->opponent_id == $user_id)
            return 'opponent';
        return NULL;
    }

    /**
     * Find opponent ID
     * @param $user_id
     * @return null|string
     */
    public function getOpponentId($user_id){
        if($this->user_id == $user_id)
            return $this->opponent_id;
        if($this->opponent_id == $user_id)
            return $this->user_id;
        return NULL;
    }

    /**
     * Get subject model
     * @return ORM
     * @throws Exception
     */
    public function getSubjectLink(){
        if($this->subject_model == 'boardad')
            $model = ORM::factory('BoardAd', $this->subject_id);
        if($model->loaded())
            return HTML::anchor($model->getUri(), $this->subject, array('target'=>'_blank'));
        return $this->subject;

    }

    /**
     * Counts unreaded dialogs with new messages
     * @param $user_id
     * @return int
     */
    public static function lookForMessages($user_id){
        $unreaded = ORM::factory('UserDialog')
            ->where_open()
            ->where('user_id', '=', $user_id)
            ->and_where('user_unread', '=', 1)
            ->where_close()
            ->or_where_open()
            ->where('opponent_id', '=', $user_id)
            ->and_where('opponent_unread', '=', 1)
            ->or_where_close()
            ->count_all();
        return $unreaded;
    }
}