<?php defined('SYSPATH') or die('No direct script access.');

class Model_UserDialogMessage extends ORM
{
    protected $_table_name = 'user_dialog_messages';

    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id',
        ),
        'dialog' => array(
            'model' => 'UserDialog',
            'foreign_key' => 'dialog_id',
        ),
    );

    public function rules()
    {
        return array(
            'text' => array(
                array('not_empty'),
                array('max_length', array(':value',1024)),
            ),
        );
    }

    public function labels()
    {
        return array(
            'id' => 'ID',
            'text' => __('Текст собщения'),
            'user_id' => 'Автор',
            'sendtime' => 'Время',
        );
    }
    
    /**
     * @param Validation $validation
     * @return ORM|void
     */
    public function save(Validation $validation=NULL){
        if(!$this->sendtime)
            $this->sendtime = time();

        return parent::save($validation);
    }

    /**
     * Returns message text
     * @return mixed
     */
    public function getText(){
        return nl2br($this->text);
    }

    /**
     * Returns formatted date and time
     */
    public function getDateTime(){
        return Date::smart_datetime($this->sendtime);
    }
}