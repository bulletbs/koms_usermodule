<?php
/**
 * Created by JetBrains PhpStorm.
 * User: butch
 * Date: 09.04.13
 * Time: 23:25
 * To change this template use File | Settings | File Templates.
 */

class Model_Profile extends ORM {

    CONST AVATARS_FOLDER = '/media/upload/profiles/';
    CONST AVATAR_WIDTH = 80;
    CONST AVATAR_HEIGHT = 80;

    protected $_table_name = 'user_profiles';

    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
        ),
    );

    public function rules()
    {
        return array(
            'name' => array(
                array('not_empty'),
            ),
        );
    }

    public function labels(){
        return array(
            'id'=>'ID',
            'name'=>'Your name',
            ''=>'',
//            ''=>'',
        );
    }

    public function filters()
    {
        return array(
            'name' => array(
                array('strip_tags'),
                array('trim'),
            ),
        );
    }

    /**
     * Avatar file path
     * @return string
     */
    public function avatarPath(){
        return self::AVATARS_FOLDER . $this->user_id . ".jpg";
    }

    /**
     * Avatar file URI
     * Если не создана - отдается УРЛ заглушки
     * @return string
     */
    public function avatarUri(){
        if(is_file(DOCROOT . $this->avatarPath()))
            return self::AVATARS_FOLDER . $this->user_id . ".jpg";
        else
            return '/media/css/images/no-avatar.png';
    }

    /**
     * Saving new user avatar
     * @param $file
     */
    public function saveAvatar($file){
        if(Image::isImage($file)){
            $avatar = DOCROOT . $this->avatarPath();
            $image = Image::factory($file);
            $image->resize(self::AVATAR_WIDTH, self::AVATAR_HEIGHT, Image::INVERSE);
            $image->crop(self::AVATAR_WIDTH, self::AVATAR_HEIGHT, NULL, 0);
            $image->save($avatar);
        }
    }

    /**
     * Remove profile avatar
     */
    public function removeAvatar(){
        $avatar = DOCROOT . $this->avatarPath();
        if(is_file($avatar))
            unlink($avatar);
    }

    /**
     * Remove profile and all relations
     * @return ORM
     */
    public function delete(){
        $this->removeAvatar();
        return parent::delete();
    }
}