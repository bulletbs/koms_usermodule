<?php

/**
 * Class Auth_ORM
 *
 * Промежуточный класс для оптимизация проверки ролей пользователей
 * Одноразовая загрузка ролей для всех дальнейших проверок
 * Для актуальности, роли загружаются при каждом проходе скрипта (не кешируются)
 */
class Auth_ORM extends Kohana_Auth_ORM{

    /**
     * Get user from session
     * @param null $default
     * @return mixed
     */
    public function get_user($default = NULL){
        $user = parent::get_user($default);
        if($user !== $default)
            $user->load_roles();
        return $user;
    }

    /**
     * Check if users logged in (optional check by role)
     * @param null $role
     * @return bool
     */
    public function logged_in($role = NULL){
        $user = $this->get_user();
        if ( ! $user)
            return FALSE;
        if(!$role)
            return TRUE;
        return $user->has_role($role);
    }
}