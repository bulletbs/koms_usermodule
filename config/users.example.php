<?php defined('SYSPATH') or die('No direct script access');
return array(
    /**
     * Use related models like this:
      'unique_index' => array(
          'model' => 'ModelName',

      // for more than one field use array:
          'foreign' => array(
              'field_1',
              'field_2',
          ),

      // for one field use string:
          'foreign' => 'field_1',
      ),
     */

    'user_submodels' => array(
        'user_messages' => array(
            'model' => 'UserDialog',
            'foreign' => array(
                'user_id',
                'opponent_id',
            ),
        ),
    ),

    'mails_per_queue' => 1,
);
