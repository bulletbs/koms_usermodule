<?php defined('SYSPATH') or die('No direct script access.');?>

<div class="submenu last">
    <h2><?php echo __('Personal cabinet')?></h2>
    <ul><?foreach($menu as $_item):?>
        <li<?php echo $_item['active'] ? ' class="selected"': ''?>><?php echo HTML::anchor($_item['uri'], $_item['label'])?></li><?endforeach?>
    </ul>
</div>