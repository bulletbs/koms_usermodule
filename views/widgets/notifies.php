<ul class="tm-navbar-notifies uk-navbar-nav">
    <?if($unreaded > 0):?><li><a href="<?php echo URL::base().Route::get('messaging')->uri()?>" title="<?php echo __('Unreaded messages: :count', array(':count'=>$unreaded))?>" class="uk-text-small"><i class="uk-icon-envelope-o uk-icon-spin"></i> <?php echo $unreaded?></a> </li><?endif?>
    <li><a href="/favorites" class="<?php echo !$favorites ? 'uk-hidden' : ''?>" title="<?php echo __('Favorites')?>" id="favorite_wrapper"><i class="uk-icon-star"></i> <span class="" id="favCount"><?php echo $favorites ?></span></a></li>
</ul>