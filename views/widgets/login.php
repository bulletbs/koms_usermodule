<ul class="auth_links">
    <?if($unreaded > 0):?><li><?php echo HTML::anchor(Route::get('messaging')->uri(), '<i class="fa fa-envelope-o"></i><span>'.$unreaded.'</span>',['class'=>"login_icon_favorite", 'title'=>__('Unreaded messages: :count', array(':count'=>$unreaded))])?></li><?endif?>
    <li><a href="<?php echo Route::get('board')->uri(['action'=>'favorites'])?>" class="login_icon_favorite<?php echo !$favorites ? ' hide' : ''?>" title="<?php echo __('Favorites')?>" id="favorite_wrapper"><i class="fa fa-star"></i><span class="" id="favCount"><?php echo $favorites ?></span></a></li>
    <?if(!Auth::instance()->logged_in('login')):?>
        <li><i class="fa fa-sign-in"></i> <?php echo HTML::anchor(Route::get('auth')->uri(['action'=>'enter']), __('Sign in / sign up'))?></li>
    <?else:?>
        <?if($user->has_role('admin')):?><li><i class="fa fa-user-secret"></i> <?php echo HTML::anchor(Route::get('admin')->uri(), __('Admin panel')) ?></li><?endif?>
        <li id="profileLabel">
            <i class="fa fa-user"></i> <?php echo HTML::anchor(Route::get('board_myads')->uri(), $user->profile->name)?>
            <div class="profile-menu list-group" style="display: none;" id="profileLabelMenu">
                <a href="<?php echo Route::get('auth')->uri(['action'=>'my-ads'])?>" class="list-group-item"><i class="fa fa-home fa-fw"></i>Мои объявления</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'mail'])?>" class="list-group-item"><i class="fa fa-weixin fa-fw"></i>Мои сообщения</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'profile'])?>" class="list-group-item"><i class="fa fa-user fa-fw"></i>Редактировать  профиль</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'password'])?>" class="list-group-item"><i class="fa fa-key fa-fw"></i>Сменить пароль</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'change_mail'])?>" class="list-group-item"><i class="fa fa-envelope fa-fw"></i>Сменить email</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'remove_account'])?>" class="list-group-item"><i class="fa fa-user-times fa-fw"></i>Удалить профиль</a>
                <a href="<?php echo Route::get('auth')->uri(['action'=>'logout'])?>" class="list-group-item list-group-item-dark"><i class="fa fa-sign-out"></i><?php echo __('Logout')?></a>
            </div>
        </li>
    <?endif?>
</ul>