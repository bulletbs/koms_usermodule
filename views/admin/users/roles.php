<?php defined('SYSPATH') or die('No direct script access.');?>

<?foreach($advanced_data['roles'] as $role):?>
    <label class="checkbox"><?= Form::checkbox('roles[]', $role->id, $model->has('roles', $role->id))?>&nbsp;<?= $role->description ?></label>
<?endforeach;?>