<?php defined('SYSPATH') or die('No direct script access.');?>
<html>
<head>
    <title></title>
</head>
<body>
<b>Здравствуйте!</b><br />
<br /><br />
<?php echo nl2br($text)?>
<br /><br />
С уважением,<br />
Администрация сайта <?php echo $site_name ?><br>
---<br/>
Если Вы не желаете больше получать наши письма - перейдите по ссылке ниже.<br>
<a href="http://<?php echo $server_name ?>/<?php echo $unsubscribe_link?>">Отписаться от рассылки</a>
</body>
</html>