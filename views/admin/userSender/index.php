<?php defined('SYSPATH') or die('No direct script access.');?>

<?php echo Widget::load('adminUserMenu')?>

<div class="pull-right">
    <a href="/admin/userSender/sendtest" data-bb="confirm"  class="btn btn-success"><?php echo __('Test (to :email)', array(':email'=>$admin_email))?></a>
    <?if($last > 0):?><a href="/admin/userSender/send" data-bb="confirm"  class="btn btn-success"><?php echo __('Send next mail pack (:count)', array(':count'=>$stepcount))?></a><?endif?>
    <a href="/admin/userSender/create" data-bb="confirm"  class="btn btn-primary"><?php echo __('Create mailer queue')?></a>
</div>
<h3><?php echo __('Mailer')?></h3>
<div class="clearfix"></div>
<div class="well">
 <b><?php echo __('Emails in mailer base')?></b> <?= $total ?><br />
 <b><?php echo __('Last to send') ?></b> <?= $last ?><br />
</div>
<h3><?php echo __('Letter content')?></h3>
<div class="well">
    <?php echo Form::open()?>
    <div  class="form-group">
        <?php echo Form::label('mailsubj', __('Mail subject'), array('class'=>'control-label'))?>
        <?php echo Form::input('mailsubj', $mailsubj, array('class'=>'form-control'))?>
    </div>
    <div  class="form-group">
        <?php echo Form::label('mailtext', __('Mail content'), array('class'=>'control-label'))?>
        <?php echo Form::textarea('mailtext', $mailtext, array('class'=>'form-control'))?>
    </div>
    <?= Form::button('submit',__('Save'),array('type'=>'submit', 'class'=>'btn btn-primary'))?>
    <?= Form::button('cancel',__('Cancel'),array('class'=>'btn'))?>
    <?php echo Form::close()?>
</div>