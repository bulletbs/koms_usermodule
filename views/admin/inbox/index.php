<?php defined('SYSPATH') or die('No direct script access.');?>

<?php echo Widget::load('adminUserMenu')?>

<h2><?php echo __('User messages') ?></h2>

<?= $pagination->render()?>

<?php echo Form::open(URL::site( $crud_uri.'/multi'))?>
<?if(count($items)):?>
    <div class="pull-right">
        <?if(count($items)):?><?php echo Form::button('delete_all', __('Delete selected'), array('class'=>'btn btn-danger', 'data-bb'=>'confirm'))?><?endif?>
    </div>
    <div class="clearfix"></div>
    <div class="row">&nbsp;</div>
<?endif;?>

<div class="well">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th><?php echo $labels['subject']?></th>
            <th><?php echo $labels['last_message_time']?></th>
            <th></th>
            <th><?php echo $labels['cnt']?></th>
            <th><?php echo __('Operations')?></th>
            <th><input type="checkbox" value="1" id="toggle_checkbox"></th>
        </tr>
        </thead>
        <?if(!count($items)):?>
        <tr><td colspan="4"><?php echo __('Nothing found')?></td></tr>
        <?endif;?>
        <? foreach($items as $item): ?>
        <tr>
            <td><?php echo $item->id ?></td>
            <td><?php echo $item->getSubjectLink() ?></td>
            <td><small><?php echo $item->getLastTime() ?></small></td>
            <td>
                <?php echo __(' by user ') . $item->user_name ?>
                <?php echo HTML::anchor($user_uri.'/userin/'.$item->user_id, '<i class="glyphicon glyphicon-log-in"></i>', array('target'=>'_blank', 'class'=>'', 'title'=>__('Login as user')))?>
                <br>
                <?php echo __(' to user ') . $item->opponent_name ?>
                <?php echo HTML::anchor($user_uri.'/userin/'.$item->opponent_id, '<i class="glyphicon glyphicon-log-in"></i>', array('target'=>'_blank', 'class'=>'', 'title'=>__('Login as user')))?>
            </td>
            <td><?php echo $item->cnt ?></td>
            <td style="width: 150px;">
                <div class="btn-group">
                    <a href="<?=URL::site( $crud_uri.'/dialog/'.$item->id . URL::query())?>" class='btn btn-inverse' title='<?=__('Open')?>'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a data-bb="confirm" href="<?=URL::site( $crud_uri.'/delete/'.$item->id . URL::query())?>" class='btn btn-inverse' title='<?=__('Delete')?>'><i class="glyphicon glyphicon-trash"></i></a>
                </div>
            </td>
            <td><input type="checkbox" name="operate[]" value="<?php echo $item->id?>"></td>
        </tr>
        <? endforeach; ?>
    </table>
    <?php echo Form::close()?>
</div>