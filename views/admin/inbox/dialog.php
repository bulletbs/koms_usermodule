<?php defined('SYSPATH') or die('No direct script access.');?>

<?php echo Widget::load('adminUserMenu')?>

<div class="pull-right"><a href="<?php echo Request::current()->referrer() ?>" class="btn btn-success"><?php echo __('Close dialog')?></a></div>
<h3><?php echo $dialog->subject?></h3>
<div class="clearfix"></div>

<?foreach($messages as $message):?>
<div class="well">
    <b><?php echo $dialog->getUserName($message->user_id) ?> / <?php echo $message->getDateTime()?></b><br>
    <?php echo $message->getText() ?>
</div>
<?endforeach?>