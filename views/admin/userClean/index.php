<?php defined('SYSPATH') or die('No direct script access.');?>

<?php echo Widget::load('adminUserMenu')?>
<h3>Очистить базу пользователей</h3>
<div class="clearfix"></div>
<div class="well">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
            Пользователей без объявлений: <?php echo $counts['no_ads']; ?><br>
            <?php echo HTML::anchor(Request::initial()->route()->uri(array('controller'=> 'userClean', 'action'=>'cleanNoAds')), 'Очистить 5000', array('class'=>'btn btn-warning', 'data-bb'=>'confirm'))?>
        </div>
<!--        <div class="col-lg-5">-->
<!--            Пользователей без авторизации: --><?php //echo $counts['no_auths']; ?><!--<br>-->
<!--            --><?php //echo HTML::anchor(Request::initial()->route()->uri(array('controller'=> 'userClean', 'action'=>'cleanNoAuths')), 'Очистить', array('class'=>'btn btn-warning', 'data-bb'=>'confirm'))?>
<!--        </div>-->
        <div class="col-lg-1"></div>
    </div>
</div>