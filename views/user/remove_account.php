<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?= __('Delete profile')?></h1>

<?php echo __('You account and all of data added by you will be removed from :project', array(':project'=>$cfg['project']['name']))?>!
<?=Form::open('', array('class' => 'pure-form  pure-form-stacked', 'id'=>'deleteForm'))?>
<fieldset>
    <br>
    <?php echo Form::submit('delete', __('Delete profile'), array('class' => 'pure-button pure-button-error', 'id'=>'deleteButton'));  ?>
    <?php echo HTML::anchor(Route::get('auth')->uri(array('action'=>'profile')), __('Cancel'), array('class' => 'pure-button pure-button-primary'));  ?>
</fieldset>
<br>
<h3 style="color: red;"><?php echo __('Attention! This action can\'t be undone')?></h3>
<?=Form::close()?>
<script type="text/javascript">
$(function(){
    $('#deleteForm').submit(function(e){
        e.preventDefault();
        if(confirm('<?php echo __('Are you sure you want to remove your account?')?>')){
            $('#deleteForm').unbind('submit');
            $('#deleteForm').submit();
        }
    });
});
</script>
