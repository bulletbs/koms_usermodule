<?php defined('SYSPATH') OR die('No direct script access.');?>

<h1><?php echo __('Changing the password')?></h1>

<?if($sended):?>
<?php echo __('A message sent to your mailbox, with instructions to reset your password') ?>

<?else:?>
    <?= Form::open('', array('class' => 'pure-form  pure-form-aligned'))?>
    <fieldset>
        <legend><?php echo __('Enter your e-mail, which you have registered your profile')?></legend>
        <?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
        <?=Form::label('username', __('E-mail'));  ?>
        <?=Form::input('username', $data['username']);  ?>
        <?= Form::submit('login', __("Change password"), array('class' => 'pure-button pure-button-primary'))?>
    </fieldset>
    <?= Form::close()?>
<?endif?>