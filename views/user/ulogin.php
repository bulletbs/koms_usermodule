<?php defined('SYSPATH') or die('No direct script access.');?>

<form class="pure-form pure-form-aligned">
<fieldset>
    <legend>Вход через социальные сети</legend>
    <?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
    <?php echo $buttons;?>
    <p>Теперь пользователи популярных сайтов («Вконтакте», Facebook, Twitter, «Одноклассники», Mail.ru, Google) могут авторизоваться у нас без регистрации и ввода пароля. То есть если у вас уже есть логин на одном из этих сайтов, вы можете войти к нам с помощью этого сайта.</p>
</fieldset>
</form>