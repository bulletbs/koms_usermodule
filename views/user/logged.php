<?php defined('SYSPATH') or die('No direct script access.');?>
<h2 class="blockhead"><?= __('You has been successfully logged')?></h2>
<?= __('Hello')?>, <?= $user->username?>!<br>
<?= __('Last visit since')?>: <?= date("d.m.Y H:i:s", $user->last_login)?><br>
<?= HTML::anchor(URL::site('user/logout'), __('Logout'))?>