<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?php echo __('Changing the password')?></h1>

<?if($user):?>
<?= Form::open('', array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <legend><?php echo __('Enter your new password')?></legend>
    <?if(isset($errors) && count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
    <?=Form::label('password', __('Password'));  ?>
    <?=Form::password('password');  ?>
    <?=Form::label('password_confirm', __('Password confirm'));  ?>
    <?=Form::password('password_confirm');  ?>
    <br>
    <?= Form::submit('login', __("Change password"), array('class' => 'pure-button pure-button-primary'))?>
</fieldset>
<?= Form::close()?>

<?else:?>
К сожалению ссылка по которой вы перешли устарела.<br>
Возможно вы уже сменили пароль по этой ссылке. Попробуйте отправить запрос повторно.<br>
<br>
<a href="/user/changepass" class="pure-button pure-button-primary">Сменить пароль</a>
<?endif?>