<?php defined('SYSPATH') or die('No direct script access.');?>

<form class="pure-form">
    <fieldset>
    <legend><?php echo  __('Register')?></legend>
    При подаче объявления и отправке сообщений пользователям сайта регистрация происходит автоматически, на указаный Вами e-mail.<br />
    Если Вы желаете зарегистрироваться без подачи объявления то перейдите по ссылке ниже.<br>
    <br>
    <a href="/register" class="pure-button pure-button-primary">Регистрация</a>
    </fieldset>
</form>