<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?= __('Change email')?></h1>
<?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
<?= Flash::render('global/flash') ?>
<?=Form::open('', array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <?=Form::label('email', __('New email'), array('class' => 'control-label'));  ?>
    <?=Form::input('email', null, array('class' => 'input-large'));  ?>
    <?=Form::label('email_confirm', __('Email confirm'), array('class' => 'control-label'));  ?>
    <?=Form::input('email_confirm', null, array('class' => 'input-large'));  ?>
    <br>
    <?=Form::submit('change', __('Change email'), array('class' => 'pure-button pure-button-primary'));  ?>
</fieldset>
<?=Form::close()?>