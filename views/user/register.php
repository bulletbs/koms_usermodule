<?php defined('SYSPATH') or die('No direct script access.');?>
<h1><?php echo __('Register')?></h1>
<?=Form::open('/register', array('class' => 'pure-form pure-form-stacked'))?>
<fieldset>
    <?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
    <?=Form::label('name', __('Enter your name'));  ?>
    <?=Form::input('name', $data['name'], array('class' => ''));  ?>
    <?=Form::label('username', __('Enter your email'));  ?>
    <?=Form::input('email', $data['email'], array('class' => ''));  ?>
    <?=Form::label('username', __('Password'));  ?>
    <?=Form::password('password', null, array('class' => ''));  ?>
    <?=Form::label('username', __('Password confirm'));  ?>
    <?=Form::password('password_confirm', null, array('class' => ''));  ?>
    <?php echo Captcha::instance(); ?>
    <br>
    <?=Form::submit('register', __('Register'), array('type'=>'submit', 'class' => 'pure-button pure-button-primary'));  ?>
<?=Form::close()?>
