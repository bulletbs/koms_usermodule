<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?= __('Profile')?></h1>

<?= Flash::render('global/flash') ?>
<div class="pure-g">
    <div class="pure-u-2-3">
        <div class="enter_col enter_first">
        <?=Form::open('', array('class' => 'pure-form  pure-form-stacked', 'enctype' => 'multipart/form-data'))?>
        <?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
        <fieldset>
            <legend><?php echo __('User info')?></legend>
            <?= Form::label('name', __('User Name'), array('class'=>'control-label')) ?>
            <?= Form::input('name', $profile->name, array('class'=>'')) ?>
            <?= Form::label('email', __('E-mail'), array('class' => 'control-label'));  ?>
            <?= Form::input('email', $user->email, array('class' => 'input-large', 'disabled'=>'disabled'));  ?>
            <?= Form::label('no_mails', Form::checkbox('no_mails', 1, $user->no_mails == 1).' '. __('Do not send me emails'), array('class' => 'control-label'));  ?>

            <?= Form::label('city_id', 'Регион')?>
            <?= Form::hidden('city_id', Arr::get($_POST,'city_id') , array('id'=>'city_id')) ?>
            <?= Form::select('region', $regions, Arr::get($_POST,'region', $region), array('class'=>isset($errors['city_id']) ? 'error-input': '', 'id'=>'region'))?>
            <span id="subRegion"><?php echo !empty($cities) ? '&nbsp;&nbsp;<b>&raquo;</b>&nbsp;&nbsp;'.$cities : '' ?></span>

            <?= Form::label('address', __('Address'), array('class' => 'control-label'));  ?>
            <?= Form::input('address', $profile->address, array('class' => 'input-large'));  ?>
            <?= Form::label('phone', __('Phone'), array('class' => 'control-label'));  ?>
            <?= Form::input('phone', $profile->phone, array('class' => 'input-large'));  ?>
            <?= Form::label('avatar', __('Change avatar'), array('class' => 'control-label'));  ?>
            <?= HTML::image($profile->avatarUri()) ?><br>
            <?= Form::file('avatar', array('class' => 'input-large'));  ?>
            <br><br>
            <?=Form::submit('update', __('Update profile'), array('class' => 'pure-button pure-button-primary'));  ?>
        </fieldset>
        <?=Form::close()?>
       </div>
    </div>
    <div class="pure-u-1-3">
        <div class="enter_col enter_last">
        <form class="pure-form pure-form-aligned">
            <fieldset>
                <legend><?php echo __('Related social network account')?></legend>
                <table class="pure-table">
                <? if (isset($networks) && count($networks) > 0):?>
                    <thead>
                        <tr><th>Привязанные профили</th></tr>
                    </thead>
                    <?foreach ($networks as $n):?>
                    <tr>
                        <td>
                            <a href='/delete_social/<?php echo urlencode($n['id']) ?>' class='pure-button  pure-button-error' title='<?php echo  __('Remove social network account') ?>'><i class=" fa fa-trash-o"></i> <?php echo __('Delete')?></a>
                            <a href='<?php echo $n['identity']?>' target='_blank' class=''><?php echo ucfirst($n['network']) ?></a>
                        </td>
                    </tr>
                    <?endforeach?>
                <?endif?>
                    <tr><td>
                        Добавить другие аккаунты:
                        <br />
                        <?=$ulogin;?>
                    </td></tr>
                </table>
            </fieldset>
        </form>
        </div>
    </div>
</div>