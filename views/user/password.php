<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?= __('Change password')?></h1>
<?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
<?= Flash::render('global/flash') ?>
<?=Form::open('', array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <?=Form::label('password', __('New Password'), array('class' => 'control-label'));  ?>
    <?=Form::password('password', null, array('class' => 'input-large'));  ?>
    <?=Form::label('password_confirm', __('Password confirm'), array('class' => 'control-label'));  ?>
    <?=Form::password('password_confirm', null, array('class' => 'input-large'));  ?>
    <br>
    <?=Form::submit('change', __('Change password'), array('class' => 'pure-button pure-button-primary'));  ?>
</fieldset>
<?=Form::close()?>