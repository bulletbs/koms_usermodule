<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?php echo __('Registration confirm')?></h1>

К сожалению ссылка по которой вы перешли устарела.<br>
Попробуйте авторизироваться повторно, возможно регистрация уже подтверждена.<br>
<a href="/enter" class="pure-button pure-button-primary">Войти</a>