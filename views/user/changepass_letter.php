<?php defined('SYSPATH') or die('No direct script access.');?>

<html>
<head>
    <title></title>
</head>
<body>
<b>Здравствуйте!</b><br />
<br />
Вы или кто-то от Вашего имени запросил смену пароля<br />
Если Вы запрашивали новый пароль, просто проигнорируйте это сообщение.<br />
<br />
---------------------<br />
Для смены пароля перейдите по ссылке: <a href="http://<?php echo $server_name ?>/<?php echo $changepass_link ?>"><?php echo __('Change password')?></a><br />
Внимание! Указаная ссылка действительна только в течении 14 дней с момента получения<br />
---------------------<br />
<br />
<br />
С уважением,<br />
Администрация сайта <?php echo $site_name ?>
</body>
</html>