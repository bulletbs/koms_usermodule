<?php defined('SYSPATH') or die('No direct script access.');?>


<?= Form::open('/enter', array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <legend><?php echo __('Sign in form')?></legend>
    <?if(count($errors)):?><?= View::factory('error/validation', array('errors' => $errors))->render()?><?endif;?>
    <?=Form::label('username', __('E-mail'));  ?>
    <?=Form::input('username', $data['username']);  ?>
    <?=Form::label('password', __('Password'));  ?>
    <?=Form::password('password', null);  ?>
    <?=Form::label('remember', Form::checkbox('remember', 1,  !isset($data['remember']) || $data['remember'] === NULL ? FALSE : TRUE) .' '. __('Remember me') );  ?>
    <br>
    <?= Form::submit('login', __("Sign in"), array('class' => 'pure-button pure-button-primary'))?>
    &nbsp;<a href="/user/changepass"><?php echo __('Forgot password?')?></a>
</fieldset>
<?= Form::close()?>