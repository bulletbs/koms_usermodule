<?php defined('SYSPATH') or die('No direct script access.');?>

<h1><?php echo __('Enter site')?></h1>
<div class="pure-alert pure-alert-primary">
    Если у Вас возникли <b>проблемы со входом в личный кабинет</b>, мы рекомендуем очистить cookie.<br>
    В случае, если Вы не знаете как самостоятельно удалить cookie нашего сайта, <b>воспользуйтесь инструкцией</b> которую можно найти <a href="https://yandex.ru/support/common/browsers-settings/browsers-cookies.xml" target="_blank">здесь</a><br>
    Выберите свой браузер из списка и следуйте инструкциям из раздела "Удалить все файлы cookie".
</div>

<?= Flash::render('global/flash') ?>
<div class="pure-g">
    <div class="pure-u-1-3">
        <div class="enter_col enter_first"><?php echo $login_form; ?></div>
    </div>
    <div class="pure-u-1-3">
        <div class="enter_col"><?php echo $social_buttons; ?></div>
    </div>
    <div class="pure-u-1-3">
        <div class="enter_col enter_last"><?php echo $register_text; ?></div>
    </div>
</div>