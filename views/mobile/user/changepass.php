<?php defined('SYSPATH') OR die('No direct script access.');?>

<h1 class="uk-h2"><?php echo __('Changing the password')?></h1>

<?if($sended):?>
<?php echo __('A message sent to your mailbox, with instructions to reset your password') ?>

<?else:?>
    <?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
    <?= Form::open('', array('class' => 'uk-form  uk-form-aligned'))?>
    <fieldset>
        <legend><?php echo __('Enter your e-mail, which you have registered your profile')?></legend>
        <div class="uk-form-row">
            <?=Form::label('username', __('E-mail'), array('class' => 'uk-form-label'));  ?>
            <?=Form::input('username', $data['username']);  ?>
        </div>
        <div class="uk-margin-top">
            <?= Form::button('login', __("Change password"), array('class' => 'uk-button uk-button-primary'))?>
        </div>
    </fieldset>
    <?= Form::close()?>
<?endif?>