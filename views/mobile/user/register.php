<?php defined('SYSPATH') or die('No direct script access.');?>

<h1 class="uk-h2"><?php echo  __('Sign up form')?></h1>

<?=Form::open('/register', array('class'=>'uk-form uk-form-stacked'))?>
<fieldset>
    <?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
    <div class="uk-form-row">
        <?=Form::label('name', __('Enter your name'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls"><?=Form::input('name', $data['name'], array('class' => ''));  ?></div>
    </div>
    <div class="uk-form-row">
        <?=Form::label('username', __('Enter your email'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls"><?=Form::input('email', $data['email'], array('class' => ''));  ?></div>
    </div>
    <div class="uk-form-row">
        <?=Form::label('username', __('Password'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls"><?=Form::password('password', null, array('class' => ''));  ?></div>
    </div>
    <div class="uk-form-row">
        <?=Form::label('username', __('Password confirm'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls"><?=Form::password('password_confirm', null, array('class' => ''));  ?></div>
    </div>
    <div class="uk-form-row">
        <?php echo Captcha::instance(); ?>
    </div>
    <?=Form::button('regbutton', __('Register'), array('type'=>'submit', 'class' => 'uk-button uk-button-primary uk-margin-top'));  ?>
    <?=Form::hidden('register', 1);  ?>
</fieldset>
<?=Form::close()?>
