<?php defined('SYSPATH') or die('No direct script access.');?>

<h1 class="uk-h2" xmlns="http://www.w3.org/1999/html"><?= __('Profile')?></h1>

<?= Flash::render('mobile/flash/flash') ?>
<?=Form::open('', array('class' => 'uk-form  uk-form-stacked', 'enctype' => 'multipart/form-data'))?>
<?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
<fieldset>
    <legend><?php echo __('User info')?></legend>
    <div class="uk-form-row">
        <?= Form::label('name', __('User Name'), array('class'=>'uk-form-label')) ?>
        <?= Form::input('name', $profile->name, array('class'=>'')) ?>
    </div>
    <div class="uk-form-row">
        <?= Form::label('email', __('E-mail'), array('class' => 'uk-form-label'));  ?>
        <?= Form::input('email', $user->email, array('class' => 'input-large', 'disabled'=>'disabled'));  ?>
        <div class="uk-margin-top uk-text-small"><?= Form::label('no_mails', Form::checkbox('no_mails', 1, $user->no_mails == 1).' '. __('Do not send me emails'), array('class' => ''));  ?></div>
    </div>
    <div class="uk-form-row">
        <?= Form::label('city_id', 'Регион', array('class' => 'uk-form-label'))?>
        <?= Form::hidden('city_id', Arr::get($_POST,'city_id') , array('id'=>'city_id')) ?>
        <?= Form::select('region', $regions, Arr::get($_POST,'region', $region), array('class'=>isset($errors['city_id']) ? 'error-input': '', 'id'=>'region'))?>
        <span id="subRegion"><?php echo !empty($cities) ? '&nbsp;&nbsp;<b>&raquo;</b>&nbsp;&nbsp;'.$cities : '' ?></span>
    </div>
    <div class="uk-form-row">
        <?= Form::label('address', __('Address'), array('class' => 'uk-form-label'));  ?>
        <?= Form::input('address', $profile->address, array('class' => 'input-large'));  ?>
    </div>
    <div class="uk-form-row">
        <?= Form::label('phone', __('Phone'), array('class' => 'uk-form-label'));  ?>
        <?= Form::input('phone', $profile->phone, array('class' => 'input-large'));  ?>
    </div>
    <div class="uk-form-row">
        <?= Form::label('avatar', __('Change avatar'), array('class' => 'uk-form-label'));  ?>
        <?= HTML::image($profile->avatarUri()) ?><br>
        <?= Form::file('avatar', array('class' => 'input-large'));  ?>
    </div>
    <div class="uk-form-row uk-margin-top">
        <?=Form::button('update', __('Update profile'), array('class' => 'uk-button uk-button-primary'));  ?>
    </div>
    <hr class="uk-divider">
    <div class="uk-form-row">
        <div class="uk-panel-box">
            <legend><?php echo __('Related social network account')?></legend>
            <? if (isset($networks) && count($networks) > 0):?>
            <div>
                Привязанные профили:<br>
                <?foreach ($networks as $n):?>
                    <a href='/delete_social/<?php echo urlencode($n['id']) ?>' class='uk-button uk-button-danger' title='<?php echo  __('Remove social network account') ?>'><i class="uk-icon-trash-o"></i> <?php echo __('Delete')?></a>
                    <a href='<?php echo $n['identity']?>' target='_blank' class=''><?php echo ucfirst($n['network']) ?></a>
                <?endforeach?>
            </div>
            <hr class="uk-divider">
            <?endif?>
            <div>
                Добавить другие аккаунты:<br />
                <?=$ulogin;?>
            </div>
        </div>
    </div>
</fieldset>
<?=Form::close()?>