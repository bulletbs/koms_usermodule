<?php defined('SYSPATH') or die('No direct script access.');?>
<h1 class="uk-h2"><?php echo __('Enter site')?></h1>
<div class="uk-alert uk-alert-primary">
    Если у Вас возникли <b>проблемы со входом в личный кабинет</b>, мы рекомендуем очистить cookie.<br>
    В случае, если Вы не знаете как самостоятельно удалить cookie нашего сайта, <b>воспользуйтесь инструкцией</b> которую можно найти <a href="https://yandex.ru/support/common/browsers-settings/browsers-cookies.xml" target="_blank">здесь</a><br>
    Выберите свой браузер из списка и следуйте инструкциям из раздела "Удалить все файлы cookie".
</div>
<?= Flash::render('mobile/flash/flash') ?>
<div class="uk-grid uk-form uk-form-stacked uk-grid-margin uk-grid-width-1-1 uk-grid-width-large-1-3"  data-uk-grid-margin data-uk-grid-match>
    <div><div class="uk-panel-box"><?php echo $login_form; ?></div></div>
    <div><div class="uk-panel-box"><?php echo $social_buttons; ?></div></div>
    <div><div class="uk-panel-box"><?php echo $register_text; ?></div></div>
</div>