<?php defined('SYSPATH') or die('No direct script access.');?>

<?= Form::open('/enter', array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <legend><?php echo __('Sign in form')?></legend>
    <?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
    <div class="uk-form-row">
        <?=Form::label('username', __('E-mail'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls">  <?=Form::input('username', $data['username']);  ?></div>
    </div>
    <div class="uk-form-row">
        <?=Form::label('password', __('Password'), array('class'=>'uk-form-label'));  ?>
        <div class="uk-form-controls"><?=Form::password('password', null);  ?></div>
        <?=Form::label('remember', Form::checkbox('remember', 1,  !isset($data['remember']) || $data['remember'] === NULL ? FALSE : TRUE) .' '. __('Remember me') );  ?>
    </div>
    <div class="uk-margin-top">
        <?= Form::button('loginbut', __("Sign in"), array('class' => 'uk-button uk-button-primary'))?>
        <?= Form::hidden('login', 1)?>
        &nbsp;<a href="/user/changepass"><?php echo __('Forgot password?')?></a>
    </div>
</fieldset>
<?= Form::close()?>