<?php defined('SYSPATH') or die('No direct script access.');?>

<h1 class="uk-h2"><?= __('Change email')?></h1>
<?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
<?= Flash::render('global/flash') ?>
<?=Form::open('', array('class' => 'uk-form  uk-form-stacked'))?>
    <div class="uk-form-row">
        <?=Form::label('email', __('New email'), array('class' => 'uk-form-label'));  ?>
        <?=Form::input('email', null, array('class' => ''));  ?>
    </div>
    <div class="uk-form-row">
        <?=Form::label('email_confirm', __('Email confirm'), array('class' => 'uk-form-label'));  ?>
        <?=Form::input('email_confirm', null, array('class' => ''));  ?>
    </div>
    <div class="uk-margin-top">
        <?=Form::button('change', __('Change email'), array('class' => 'uk-button uk-button-primary'));  ?>
    </div>
<?=Form::close()?>