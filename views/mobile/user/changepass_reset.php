<?php defined('SYSPATH') or die('No direct script access.');?>

<h1 class="uk-h2"><?php echo __('Changing the password')?></h1>

<?if($user):?>
<?if(isset($errors) && count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
<?= Form::open('', array('class' => 'uk-form  uk-form-stacked'))?>
<fieldset>
    <legend><?php echo __('Enter your new password')?></legend>
    <div class="uk-form-row">
        <?=Form::label('password', __('Password'), array('class' => 'uk-form-label'));  ?>
        <?=Form::password('password');  ?>
    </div>
    <div class="uk-form-row">
        <?=Form::label('password_confirm', __('Password confirm'), array('class' => 'uk-form-label'));  ?>
        <?=Form::password('password_confirm');  ?>
    </div>
    <div class="uk-margin-top">
        <?= Form::submit('login', __("Change password"), array('class' => 'pure-button pure-button-primary'))?>
    </div>
</fieldset>
<?= Form::close()?>

<?else:?>
К сожалению ссылка по которой вы перешли устарела.<br>
Возможно вы уже сменили пароль по этой ссылке. Попробуйте отправить запрос повторно.<br>
<br>
<a href="/user/changepass" class="pure-button pure-button-primary">Сменить пароль</a>
<?endif?>