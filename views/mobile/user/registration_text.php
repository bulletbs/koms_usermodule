<?php defined('SYSPATH') or die('No direct script access.');?>

<form class="pure-form">
    <fieldset>
    <legend><?php echo  __('Register')?></legend>
    <p>При подаче объявления и отправке сообщений пользователям сайта <b>регистрация происходит автоматически</b>, на указаный Вами e-mail.</p>
    <p>Если Вы желаете зарегистрироваться без подачи    объявления то перейдите по ссылке ниже.</p>
    <br>
    <a href="/register" class="uk-button uk-button-primary uk-align-center">Регистрация</a>
    </fieldset>
</form>