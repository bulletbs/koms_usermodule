<?php defined('SYSPATH') or die('No direct script access.');?>

<h1 class="uk-h2"><?= __('Change password')?></h1>
<?if(count($errors)):?><?= View::factory('mobile/error/validation', array('errors' => $errors))->render()?><?endif;?>
<?= Flash::render('mobile/flash/flash') ?>
<?=Form::open('', array('class' => 'uk-form  uk-form-stacked'))?>
<fieldset>
    <div class="uk-form-row">
    <?=Form::label('password', __('New Password'), array('class' => 'uk-form-label'));  ?>
    <?=Form::password('password', null, array('class' => 'input-large'));  ?>
    </div>
    <div class="uk-form-row">
    <?=Form::label('password_confirm', __('Password confirm'), array('class' => 'uk-form-label'));  ?>
    <?=Form::password('password_confirm', null, array('class' => 'input-large'));  ?>
    </div>
    <div class="uk-margin-top">
        <?=Form::button('change', __('Change password'), array('class' => 'uk-button uk-button-primary'));  ?>
    </div>
</fieldset>
<?=Form::close()?>