<?php defined('SYSPATH') or die('No direct script access.');?>
<h1 class="uk-h2"><?php echo __('My messages')?></h1>
<?= Flash::render('mobile/flash/flash') ?>
<ul class="uk-tab uk-margin-top  uk-text-small">
    <li class="<?php echo $active == 'inbox' ? ' uk-active'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'index')), __('Received'), array('class'=>''))?></li>
    <li class="<?php echo $active == 'sent' ? ' uk-active'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'sent')), __('Sent'), array('class'=>''))?></li>
    <li class="<?php echo $active == 'archive' ? ' uk-active'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'archive')), __('Archived'), array('class'=>''))?></li>
</ul>

<table class="uk-table uk-table-striped uk-text-small">
<thead>
<tr>
    <th class="dialogs_id">ID</th>
    <th class="dialogs_title"><?php echo __('Title') ?></th>
    <th class="dialogs_last"><?php echo __('Last message') ?></th>
    <th class="dialogs_operations"><?php echo __('Operations') ?></th>
</tr>
</thead>
<tbody>
<?if(count($dialogs)):?>
<?foreach($dialogs as $_dialog):?>
    <tr>
        <td><?php echo $_dialog->id ?></td>
        <td><?php echo HTML::anchor($_dialog->getUri().'#form', $_dialog->getTitle(), array('class'=>$_dialog->haveNewMessages($myId) ? 'dialog_unreaded' : '', 'title'=>$_dialog->haveNewMessages($myId) ? __('Unreaded messages') : '')) ?></td>
        <td><?php echo $_dialog->getLastTime() . __(' by user ') . $_dialog->getLastName($myId) ?> <?if($_dialog->haveNewMessages($myId)):?><span class="uk-badge uk-badge-danger" title="<?php echo __('Unreaded messages')?>">!</span><?endif?></td>
        <td>
            <?if($active == 'archive'):?> <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'unpack', 'id'=>$_dialog->id)), '<i class="uk-icon-archive"></i>', array('title'=>__('Unarchive dialog'), 'class'=>'uk-button uk-button-mini')) ?>
            <?else:?> <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'dialog', 'id'=>$_dialog->id)).'#form', '<i class="uk-icon-mail-reply"></i>', array('title'=>__('Write answer'), 'class'=>'uk-button uk-button-mini')) ?>
            <?endif?>
            <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=> $active == 'archive' ? 'delete': 'pack', 'id'=>$_dialog->id)), '<i class="uk-icon-trash-o"></i>', array('title'=> __($active=='archive' ? 'Delete dialog' : 'Archive dialog'), 'class'=>'uk-button uk-button-mini uk-button-danger')) ?>
        </td>
    </tr>
<?endforeach;?>
<?else:?>
    <tr>
        <td colspan="4" class="alcenter"><b><?php echo __('You have no messages')?></b></td>
    </tr>
<?endif;?>
</tbody></table>

<script type="text/javascript">
    $('.pure-button-danger').on('click', function(e){
        if(!confirm('<?php echo __('Are you sure?')?>'))
            e.preventDefault();
    });
</script>