<?php defined('SYSPATH') or die('No direct script access.');?>
<h1 class="uk-h2">Диалог: <?php echo $dialog->subject?></h1>
<?= Flash::render('global/flash') ?>
<?if(count($messages)):?>
<?foreach($messages as $_mess):?>
<article class="tm-comment uk-comment uk-margin-top<?php echo $_mess->user_id == $myId ? ' uk-comment-primary' : ''?>">
    <header class="uk-comment-header">
        <img src="<?php echo $users[$_mess->user_id]->profile->avatarUri() ?>" class="uk-comment-avatar">
        <h4 class="uk-comment-title"><?php echo $_mess->user_id == $myId ? __('Me') : $users[$_mess->user_id]->profile->name ?></h4>
        <div class="uk-comment-meta"><?php echo $_mess->getDateTime() ?></div>
    </header>
    <div class="uk-comment-body"><?php echo $_mess->getText() ?></div>
</article>
<?endforeach;?>
<?endif;?>

<?=Form::open(Route::get('messaging')->uri(array('action'=>'send', 'id'=>$dialog->id)), array('class' => 'uk-form  uk-form-stacked uk-margin-top'))?>
<fieldset>
    <div class="uk-form-row">
        <?php echo Form::textarea('text', NULL, array('placeholder'=>__('Enter your message...'), 'class'=>'chat_input')) ?>
    </div>
    <div class="uk-form-row">
        <?php echo Form::button('update', __('Send message'), array('class' => 'uk-button uk-button-primary uk-float-right uk-margin-left')); ?>
        <?php echo HTML::anchor( Route::get('messaging')->uri(), __('Close dialog'), array('class' => 'uk-button uk-button-warning uk-float-right')) ?>
    </div>
</fieldset>
<?=Form::close()?>