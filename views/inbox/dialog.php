<?php defined('SYSPATH') or die('No direct script access.');?>
<h1>Диалог: <?php echo $dialog->subject?></h1>
<?= Flash::render('global/flash') ?>
<?if(count($messages)):?>
<ol class="chat-box">
<?foreach($messages as $_mess):?>
<li class='<?php echo $_mess->user_id == $myId ? 'me' : 'another' ?>'>
    <div class='avatar-icon'><?php echo HTML::image( $users[$_mess->user_id]->profile->avatarUri() ) ?><br></div>
    <div class='messages'>
        <p><?php echo $_mess->getText() ?></p>
        <small class="quiet"><?php echo $users[$_mess->user_id]->profile->name ?> / <?php echo $_mess->getDateTime() ?></small>
    </div>
</li><?endforeach;?>
</ol>
<?endif;?>

<!--<a name="form"></a>-->
<?=Form::open(Route::get('messaging')->uri(array('action'=>'send', 'id'=>$dialog->id)), array('class' => 'pure-form  pure-form-stacked'))?>
<fieldset>
    <?php echo Form::textarea('text', NULL, array('placeholder'=>__('Enter your message...'), 'class'=>'chat_input')) ?>
    <?php echo Form::submit('update', __('Send message'), array('class' => 'pure-button pure-button-primary right')); ?>
    <?php echo HTML::anchor( Route::get('messaging')->uri(), __('Close dialog'), array('class' => 'pure-button pure-button- right')) ?>
</fieldset>
<?=Form::close()?>