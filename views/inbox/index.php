<?php defined('SYSPATH') or die('No direct script access.');?>

<?= Flash::render('global/flash') ?>

<div class="dialog_menu pure-menu  pure-menu-horizontal">
    <ul class="pure-menu-list">
        <li class="pure-menu-item<?php echo $active == 'inbox' ? ' pure-menu-selected'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'index')), __('Received messages'), array('class'=>'pure-menu-link'))?></li>
        <li class="pure-menu-item<?php echo $active == 'sent' ? ' pure-menu-selected'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'sent')), __('Sent messages'), array('class'=>'pure-menu-link'))?></li>
        <li class="pure-menu-item<?php echo $active == 'archive' ? ' pure-menu-selected'  : ''?>"><?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'archive')), __('Archived messages'), array('class'=>'pure-menu-link'))?></li>
    </ul>
</div>

<table class="dialogs pure-table pure-table-horizontal">
<thead>
<tr>
    <th class="dialogs_id">ID</th>
    <th class="dialogs_title"><?php echo __('Title') ?></th>
    <th class="dialogs_last"><?php echo __('Last message') ?></th>
    <th class="dialogs_operations"><?php echo __('Operations') ?></th>
</tr>
</thead>
<tbody>
<?if(count($dialogs)):?>
<?foreach($dialogs as $_dialog):?>
    <tr>
        <td><?php echo $_dialog->id ?></td>
        <td><?php echo HTML::anchor($_dialog->getUri().'#form', $_dialog->getTitle(), array('class'=>$_dialog->haveNewMessages($myId) ? 'dialog_unreaded' : '', 'title'=>$_dialog->haveNewMessages($myId) ? __('Unreaded messages') : '')) ?></td>
        <td><?php echo $_dialog->getLastTime() . __(' by user ') . $_dialog->getLastName($myId) ?> <?if($_dialog->haveNewMessages($myId)):?><span class="pure-badge-error" title="<?php echo __('Unreaded messages')?>">!</span><?endif?></td>
        <td>
            <?if($active == 'archive'):?> <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'unpack', 'id'=>$_dialog->id)), '<i class="fa fa-archive"></i>', array('title'=>__('Unarchive dialog'), 'class'=>'pure-button pure-button')) ?>
            <?else:?> <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=>'dialog', 'id'=>$_dialog->id)).'#form', '<i class="fa fa-mail-reply"></i>', array('title'=>__('Write answer'), 'class'=>'pure-button pure-button')) ?>
            <?endif?>
            <?php echo HTML::anchor(Route::get('messaging')->uri(array('action'=> $active == 'archive' ? 'delete': 'pack', 'id'=>$_dialog->id)), '<i class="fa fa-trash-o"></i>', array('title'=> __($active=='archive' ? 'Delete dialog' : 'Archive dialog'), 'class'=>'pure-button pure-button-error')) ?>
        </td>
    </tr>
<?endforeach;?>
<?else:?>
    <tr>
        <td colspan="4" class="alcenter"><b><?php echo __('You have no messages')?></b></td>
    </tr>
<?endif;?>
</tbody></table>

<script type="text/javascript">
    $('.pure-button-error').on('click', function(e){
        if(!confirm('<?php echo __('Are you sure?')?>'))
            e.preventDefault();
    });
</script>