<?php defined('SYSPATH') or die('No direct script access.');

if(!Route::cache()){
    Route::set('auth', '<action>', array('action' => '(logout|profile|enter|register|password|change_mail|remove_account|remove)'))
        ->defaults(array(
        'controller' => 'user',
    ));
    Route::set('confirm_registration', 'registration_confirm/<id>/<hash>', array('id' => '[0-9]+', 'hash' => '[\d\w]{32}'))
        ->defaults(array(
            'action' => 'confirm',
            'controller' => 'user',
        ));
    Route::set('new_password', 'reset_password/<id>/<hash>', array('id' => '[0-9]+', 'hash' => '[\d\w]{32}'))
        ->defaults(array(
            'action' => 'changepass_reset',
            'controller' => 'user',
        ));
    Route::set('social_remove', 'delete_social/<id>', array('id' => '[0-9]+'))
        ->defaults(array(
            'action' => 'delete_social',
            'controller' => 'user',
        ));
    Route::set('crypt_action', 'logact/<crypt>', array('crypt' => '.*'))
        ->defaults(array(
            'action' => 'crypted_action',
            'controller' => 'userCrypt',
        ));

    Route::set('messaging', 'mail(/<action>(/<id>)(/p<page>))', array('id' => '[0-9]+', 'page' => '[0-9]+', 'action' => '(sent|archive|dialog|send|pack|unpack|delete)'))
        ->defaults(array(
            'action' => 'index',
            'controller' => 'userInbox',
        ));
}