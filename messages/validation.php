<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    'banned_domain' => ':field contain banned domain',
);